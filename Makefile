.PHONY: clean mrproper

all : cours

cours:
	$(MAKE) -C Cours/ all

clean:
	$(MAKE) -C Cours/ clean

mrproper:
	$(MAKE) -C Cours/ mrproper
