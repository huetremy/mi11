# Cours du 9 mars 2021

Systèmes temps réel critiques - Jérôme De Miras

- [Cours du 9 mars 2021](#cours-du-9-mars-2021)
  - [Bibliographie](#bibliographie)
  - [Panorama général](#panorama-général)
    - [Définition de base](#définition-de-base)
    - [Clusters](#clusters)
    - [Systèmes distribués](#systèmes-distribués)
  - [Exigences](#exigences)
    - [Exigences fonctionnelles](#exigences-fonctionnelles)
      - [Collecte de données](#collecte-de-données)
      - [Contrôle numérique](#contrôle-numérique)
      - [Interactions homme-machine](#interactions-homme-machine)
    - [Exigences temporelles](#exigences-temporelles)
    - [Exigences de fiabilité](#exigences-de-fiabilité)
  - [Classification des systèmes](#classification-des-systèmes)
    - [Hard real time vs soft real time](#hard-real-time-vs-soft-real-time)
    - [« Fail safe » contre « Fail operationnal »](#-fail-safe-contre-fail-operationnal-)
    - [Ressources adéquates contre ressources inadéquates](#ressources-adéquates-contre-ressources-inadéquates)
    - [Event triggered contre time triggered](#event-triggered-contre-time-triggered)
    - [Systèmes temps réel embarqués](#systèmes-temps-réel-embarqués)
  - [Architecture pour les systèmes temps réeel](#architecture-pour-les-systèmes-temps-réeel)

## Bibliographie

- Real-Time Systems : design principles for distribued embedded applications, Hermann Kopetz

## Panorama général

### Définition de base

- Un système informatique temps réel est un système qui doit être évalué tant sur la validité des résultats logiques qu'il produit que sur le temps qu'il met à les fournir. Il y a donc une containte temporelle en plus de la contrainte logique.
- Un système temps réel doit donc répondre aux stimuli de son environement dans un temps donné : **l'échéance**.
- En fonction des contraintes sur les échéances, un système temps réel est dit dur (hard) ou mou (soft).

### Clusters

Un système informatique temps réel est une partie d'un système plus large qui dépend du temps et se compose de clusters.

```mermaid
graph LR:
    O[Opérateur]
    S[Système informatique temps rée]
    C[Objet contrôlé]
    O --> S --> C
    C --> S --> O
```

### Systèmes distribués

Un système temps réel peut être distribué.
Il est compsoé de nœuds et de liens de communication temps réel.

## Exigences

- Exigences fonctionnelles : les fonctions de le système doit remplir
- Exigences temporelles : les contraintes de temps à respecter
- Exigences de fiabilité

### Exigences fonctionnelles

Les fonctions que doit remplir un système informatique temps réel peuvent se grouper en trois classes :

- Collecte de données
- Pilotage de l'environement
- Interactions homme machine

#### Collecte de données

Un système change d'état en fonction du temps.
Si on arrête le temps, l'état du système peut être défini par un certain nombre de valeurs (vitesse, position, couleur du feu...).

Seul un sous ensemble de ces variable est pertinent pour l'application : les **entités temps réel**.
Chaque entité TR appartient à une **sphère de contrôle** où elle peurt être modifiée.
En dehors de cette sphère, l'entité TR est uniquement observable.

Observer et collecter ces entités TR est l'exigence n°1. On parle d'**images temps réel**.
Une image TR a une validité llimitée dans le temps.
L'ensemble des images TR valides est appelé **Base de données temps réel**.

Mise à jour des entités TR :

- « Time triggered »
- « Event trigerred »

Définitions diverses :

- Toute propriété d'une entité TR qui reste valide pendant un intervalle est un **attrtibut d'état**
- Un changement d'état est un **événement**
- Une observation est un événement qui permet l'acquisition de l'état d'une entité TR à un instant donné
- L'horloge découpe la ligne de temps en portions égales, appelés *granules de temps de l'horoge*
- Les granules de temps sont séparés par des événements : les *ticks d'horloge*
- Un **trigger** est un événement qui déclenche une action

#### Contrôle numérique

=> Calculer le point de fonctionnement pour un actionneur en fonction de la BDTR

C'est une tâche répétitive et régulière :

- Problème de l'automatique
- Exigences temporelles en +

#### Interactions homme-machine

- Un système temps réel doit informer l'opérateur de l'état du système contrôlé.
- La plupart des accidents sur des applications critiques ont eu lieu à cause de l'IHM

### Exigences temporelles

Les exigences temporelles proviennent en grande partie des besoins de performance des boucles de régulation.

### Exigences de fiabilité

- Fiabilité
- Sureré
- Maintenabilité
- Disponibilité
- ...

## Classification des systèmes

### Hard real time vs soft real time

| Caractéristique                | Hard real time   | Soft real time         |
| ------------------------------ | ---------------- | ---------------------- |
| Temps de réponse               | Sans compromis   | Violation possible     |
| Performance sur pic de charge  | Prédictible      | Dégradation            |
| Vitesse de conduite            | Environnement    | Calculateur            |
| Sureté                         | Souvent critique | Non critique           |
| Taille des fichiers de données | Petit / moyen    | Large                  |
| Type de redondance             | Active           | Points de restauration |
| Intégrité des données          | Court terme      | Long terme             |
|                                |                  |                        |

### « Fail safe » contre « Fail operationnal »

Fail safe :

- En cas de panne le système peut atteindre un état sûr
- Caractéristique de l'objet et non du système informatique
- La détection d'erreur doit avoir une couvertur très large => la gestion TR améliore a disponibilité

Fail operational :

- Fonctionner quoiqu'il arrive

### Ressources adéquates contre ressources inadéquates

Un système TR critique a besoin d'être dimentionné pour le pire cas.
Cela mène a des systèmes économiquement peu enviables.

### Event triggered contre time triggered

Deux approches possibles en fonction du mode de déclenchement des communications et de l'activité de calcul :

- Event triggered : le mécanisme d'interruption permet de répondre à l'événement => ordonnancement dynamique
- Time triggered : seule la progression du temps fait évoluer le système => disposer d'un temps global et connaître à l'avance le fonctionnement du système

### Systèmes temps réel embarqués

Éléments pertinents pour le design d'un système :

- Production de masse
- Structure statique
- Interface homme machine
- Support de stockage du logiciel
- Stratégie de maintenance
- Capacité de communication

## Architecture pour les systèmes temps réeel
