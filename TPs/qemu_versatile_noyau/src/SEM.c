/*----------------------------------------------------------------------------*
 * fichier : sem.c                                                            *
 * gestion des semaphores pour le mini-noyau temps reel                       *
 *----------------------------------------------------------------------------*/

#include "SEM.h"
#include "FIFO.h"
#include "noyau.h"

/*----------------------------------------------------------------------------*
 * declaration des structures                                                 *
 *----------------------------------------------------------------------------*/

extern uint16_t _tache_c;

/*
 * structure definissant un semaphore
 */
typedef struct {
    FIFO file;
    int8_t valeur;
} SEMAPHORE;

/*----------------------------------------------------------------------------*
 * variables globales internes                                                *
 *----------------------------------------------------------------------------*/

/*
 * variable stockant tous les semaphores du systeme
 */
SEMAPHORE _sem[MAX_SEM];

/*----------------------------------------------------------------------------*
 * definition des fonctions                                                   *
 *----------------------------------------------------------------------------*/

/*
 * /!!!!\ NOTE IMPORTANTE /!!!!\
 * pour faire les verifications de file, on pourra utiliser la variable de
 * file fifo_taille et la mettre a FIFO_UNUSED dans le cas ou la file n'est pas
 * utilisee
 */

/*
 * initialise les sempaphore du systeme
 * entre  : sans
 * sortie : sans
 * description : initialise l'ensemble des semaphores du systeme
 */
void s_init(void) {
	register SEMAPHORE *s = _sem;
	register unsigned j;
		   
   
	for (j = 0; j < MAX_SEM; j++)
	{
		s[j].file.fifo_taille = FIFO_UNUSED;
	}
}

/*
 * cree un semaphore
 * entre  : valeur du semaphore a creer
 * sortie : numero du semaphore cree
 * description : cree un semaphore
 *               en cas d'erreur, le noyau doit etre arrete
 */
uint8_t s_cree(int8_t valeur) {
	register SEMAPHORE *s = _sem;
	register unsigned n = 0;

	// section critique
	_lock_();
	/* Rechercher un sem libre */
	while(s[n].file.fifo_taille != FIFO_UNUSED && n++ < MAX_SEM)
		continue;

	if (n < MAX_SEM)
	{
		fifo_init(&(s[n].file));
		s[n].valeur = valeur;
	}
	else
	{
		noyau_exit();
	}
	// fin section critique
	_unlock_();

	return(n);	   
}

/*
 * ferme un semaphore pour qu'il puisse etre reutilise
 * entre  : numero du semaphore a fermer
 * sortie : sans
 * description : ferme un semaphore
 *               en cas d'erreur, le noyau doit etre arrete
 */
void s_close(uint8_t n) {
	register SEMAPHORE *s = &_sem[n];

	// section critique
	_lock_();
	/* Vérifier sem crée sinon sortie noyau */
	if (s->file.fifo_taille == FIFO_UNUSED)
	{
		noyau_exit();
	}
	s->file.fifo_taille = FIFO_UNUSED;

	_unlock_();
	// fin section critique		   
}

/*
 * tente de prendre le semaphore dont le numero est passe en parametre
 * entre  : numero du semaphore a prendre
 * sortie : sans
 * description : prend le semaphore
 *               si echec, la tache doit etre suspendue
 *               en cas d'erreur, le noyau doit etre arrete
 */
void s_wait(uint8_t n) {

	register SEMAPHORE *s = &_sem[n];

	// section critique
	_lock_();
	// Le sémaphore n'existe pas ? => sortie noyau
	if (s->file.fifo_taille == FIFO_UNUSED)
	{
		noyau_exit();
	}
	// sinon faire ce qu'il faut en fonction de la situation
	s->valeur--;
	if(s->valeur < 0) {
		fifo_ajoute(&(s->file), _tache_c);
		dort();
	}
	
	_unlock_();
	// fin section critique			
}

/*
 * libere un semaphore
 * entre  : numero du semaphore a liberer
 * sortie : sans
 * description : libere un semaphore
 *               si des taches sont en attentes, la premiere doit etre reveillee
 *               en cas d'erreur, le noyau doit etre arrete
 */
void s_signal(uint8_t n) {
	register SEMAPHORE *s = &_sem[n];

	// section critique
	_lock_();
	// Le sémaphore n'existe pas ? => sortie noyau
	if (s->file.fifo_taille == FIFO_UNUSED)
	{
		noyau_exit();
	}
	// sinon faire ce qu'il faut en fonction de la situation
	s->valeur++;
	if(s->valeur <= 0) {
		uint8_t task;
		if (fifo_retire(&(s->file), &task) == -1)
			reveille(task);
	}
	// fin section critique		
	_unlock_();
}

int8_t s_getval(uint8_t n) {
	return _sem[n].valeur;
}