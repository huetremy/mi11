/*
 * utils.c
 *
 *  Created on: 28 mai 2020
 *      Author: jdm
 */

#include <stdint.h>

#include "noyau.h"
#include "noyau_file.h"
#include "delay.h"

/*----------------------------------------------------------------------------*
 * variables communes a toutes les procedures                                 *
 *----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*
 * fonctions de gestion des délais                                            *
 *----------------------------------------------------------------------------*/

/*
 * entrée  : nombre de tick d'attente
 * sortie  : sans
 * description : renseigne le compteur de délai de la structure _contexte de la tâche courante
 * 				 endort la tâche
 */
void delay_n_ticks(uint32_t nticks){
	_lock_();
	uint16_t t = noyau_get_tc();
	_contexte[t].cmpt = nticks;
	dort();
	_unlock_();
}

/*
 * entrée  : sans (fonction appelée dans scheduler)
 * sortie : sans
 * description : parcours l'ensemble des contextes de tâches pour vérifier celles qui sont SUSP
 * 				 pour celles dont le compteur est non nul
 * 				 	décrémente le compteur
 * 				 	remet la tache en exécution si son compteur arrive à zéro
 *
 */
void delay_process(void){
	for(uint8_t i = 0; i < MAX_TACHES_NOYAU; i++) {
		if(_contexte[i].status == SUSP && _contexte[i].cmpt != 0) {
			if( --_contexte[i].cmpt == 0) {
				_contexte[i].status = EXEC;
				file_ajoute(i);
			}
		}
	}
}



