/*
 * TERMINAL.c
 *
 *  Created on: 28 mai 2020
 *      Author: jdm
 */

#include "TERMINAL.h"

/*
 * Définition de macro pour envoi de codes d'échappement directement au terminal
 */
inline void SET_FONT_COLOR_256(uint8_t red,uint8_t green,uint8_t blue){
	printf("%s%dm",CODE_FONT_COLOR, 16 + (red * 36) + (green * 6) + blue);
}

inline void SET_FONT_COLOR(uint8_t color) {
	printf("%s%dm",CODE_FONT_COLOR, color);
}

inline void SET_BACKGROUND_COLOR_256(uint8_t red,uint8_t green,uint8_t blue){
	printf("%s%dm",CODE_BACKGROUND_COLOR, 16 + (red * 36) + (green * 6) + blue);
}

inline void SET_BACKGROUND_COLOR(uint8_t color) {
	printf("%s%dm",CODE_BACKGROUND_COLOR, color);
}

inline void SET_BOLD_FONT(uint8_t color) {
	printf(CODE_BOLD_FONT);
}

inline void SET_UNDERLINE_FONT() {
	printf(CODE_UNDERLINE_FONT);
}

inline void SET_REVERSED_FONT() {
	printf(CODE_REVERSED_FONT);
}

inline void RESET_COLOR() {
	printf(CODE_RESET_COLOR);
}

inline void SAUT_DE_LIGNE() {
	printf("\n");
}

inline void CURSOR_UP(uint16_t Nb) {
	printf("%s%d%s",CODE_ESCAPE_BASE,Nb,"A");
}

inline void CURSOR_DOWN(uint16_t Nb) {
	printf("%s%d%s",CODE_ESCAPE_BASE,Nb,"B");
}

inline void CURSOR_RIGHT(uint16_t Nb) {
	printf("%s%d%s",CODE_ESCAPE_BASE,Nb,"C");
}

inline void CURSOR_LEFT(uint16_t Nb) {
	printf("%s%d%s",CODE_ESCAPE_BASE,Nb,"D");
}

inline void SET_CURSOR_COLUMN(uint16_t Nb) {
	printf("%s%d%s",CODE_ESCAPE_BASE,Nb,"G");
}

inline void SET_CURSOR_POSITION(uint16_t n,uint16_t m) {
	printf("%s%d;%d%s",CODE_ESCAPE_BASE,n,m,"H");
}

inline void SAVE_CURSOR_POSITION() {
	printf("\x1B[s");
}

inline void RESTORE_CURSOR_POSITION() {
	printf("\x1B[u");
}

inline void CLEAR_SCREEN(uint8_t Nb) {
	printf("%s%d%s",CODE_ESCAPE_BASE,Nb,"J"); //NB 1,1 ou 2
}

inline void CLEAR_LINE(uint8_t Nb) {
	printf("%s%d%s",CODE_ESCAPE_BASE,Nb,"K"); //NB 1,1 ou 2
}



void test_colors(){
	int color;
	int green, red, blue;

	/* 0 - 15 */
	  puts("System colors:");
	  for(color = 0; color<8; color++)
	  {
		  SET_BACKGROUND_COLOR(color);
		  printf(" ");
	  }
	  RESET_COLOR();
	  SAUT_DE_LIGNE();
	  for(color = 8; color<16; color++) {
		  SET_BACKGROUND_COLOR(color);
		  printf(" ");
	  }
	  RESET_COLOR();
	  SAUT_DE_LIGNE();
	  // 16 - 231
	  puts("Color cube, 6x6x6:");
	  for(red = 0; red<6; red++) {
		  for(green = 0; green<6; green++) {
			  for(blue = 0; blue<6; blue++) {
				  color = 16 + (red * 36) + (green * 6) + blue;
				  SET_BACKGROUND_COLOR(color);
				  SET_FONT_COLOR((color+6)%256);
				  printf(" %3d", color);
			  }
			  RESET_COLOR();
			  SAUT_DE_LIGNE();
		  }
	  // 232 - 255
	  puts("Grayscale ramp:");
	  for (color = 232; color < 256; color++) {
		  SET_BACKGROUND_COLOR(color);
		  printf(" ");
	  }
	  RESET_COLOR();
	  SAUT_DE_LIGNE();
	  }
}



