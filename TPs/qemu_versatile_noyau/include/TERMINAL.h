/*
 *TERMINAL.h
 *
 *  Created on: 28 mai 2020
 *      Author: jdm
 */


#ifndef __TERMINAL_H__
#define __TERMINAL_H__

#include <stdint.h>
#include "serialio.h"

#define POS_FILE 40
#define POS_FIN_FILE 120

/*
 * Definition de chaine de caractères pour construire des codes d'échappement pour le terminal
 * Attention il faut compléter par des éléments
 */
#define CODE_FONT_COLOR "\x1B[38;5;"
#define CODE_BACKGROUND_COLOR "\x1B[48;5;"
#define CODE_RESET_COLOR "\x1B[0m"
#define CODE_BOLD_FONT "\x1B[1m"
#define CODE_UNDERLINE_FONT "\x1B[4m"
#define CODE_REVERSED_FONT "\x1B[7m"
#define CODE_ESCAPE_BASE "\x1B["

/*
 * Définition de macro pour envoi de codes d'échappement directement au terminal
 */
void SET_FONT_COLOR_256(uint8_t red,uint8_t green,uint8_t blue);

void SET_FONT_COLOR(uint8_t color);

void SET_BACKGROUND_COLOR_256(uint8_t red,uint8_t green,uint8_t blue);

void SET_BACKGROUND_COLOR(uint8_t color);

void SET_BOLD_FONT(uint8_t color);

void SET_UNDERLINE_FONT();

void SET_REVERSED_FONT();

void RESET_COLOR() ;

void SAUT_DE_LIGNE() ;

void CURSOR_UP(uint16_t Nb);

void CURSOR_DOWN(uint16_t Nb);

void CURSOR_RIGHT(uint16_t Nb);

void CURSOR_LEFT(uint16_t Nb);

void SET_CURSOR_COLUMN(uint16_t Nb);

void SET_CURSOR_POSITION(uint16_t n,uint16_t m);

void SAVE_CURSOR_POSITION() ;

void RESTORE_CURSOR_POSITION();

void CLEAR_SCREEN(uint8_t Nb);

void CLEAR_LINE(uint8_t Nb);

/*----------------------------------------------------------------------------*
 * declaration des prototypes                                                 *
 *----------------------------------------------------------------------------*/

/*
 * entrée  : sans
 * sortie : sans
 * description : code de démonstration de la palette de couleur du terminal utilisant les macro
 * définies plus haut
 */
void test_colors(void);

#endif
