/*----------------------------------------------------------------------------*
 * fichier : noyau_test.c                                                     *
 * programme de test du noyaut                                                *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include <stdlib.h>

#include "serialio.h"
#include "noyau.h"
#include "TERMINAL.h"
#include "delay.h"

TACHE	tachedefond(void);
TACHE	tacheB(void);
TACHE	tacheC(void);
TACHE	tacheD(void);


#define MAX_CARA_LIGNE 80

uint16_t pos_x = 1;
uint16_t pos_y = 10;


TACHE	tachedefond(void)
{
  SET_CURSOR_POSITION(3,1);
  puts("------> EXEC tache de fond");

  active(cree(tacheB));
  active(cree(tacheC));
  active(cree(tacheD));
  while(1){};
}

TACHE	tacheB(void)
{
  int i=0;
  SET_CURSOR_POSITION(4,1);
  puts("------> DEBUT tache B\n");
  SAVE_CURSOR_POSITION();
  while (1) {
	delay_n_ticks(5);
    if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
    SET_CURSOR_POSITION(pos_y,pos_x);
    SET_BACKGROUND_COLOR(20);
    SET_FONT_COLOR(15);
    printf("B%4d", i);
    pos_x = pos_x + 6;
    i++;
  }
}

TACHE	tacheC(void)
{
  int i=0;
  SET_CURSOR_POSITION(5,1);
  puts("------> DEBUT tache C\n");
  SAVE_CURSOR_POSITION();
  while (1) {
	  delay_n_ticks(10);
    if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
    SET_CURSOR_POSITION(pos_y,pos_x);
    SET_BACKGROUND_COLOR(30);
    SET_FONT_COLOR(15);
    printf("C%4d", i);
    pos_x = pos_x + 6;
    i++;
  }
}

TACHE	tacheD(void)
{
  int i=0;
  SET_CURSOR_POSITION(6,1);
  puts("------> DEBUT tache C\n");
  SAVE_CURSOR_POSITION();
  while (1) {
	  delay_n_ticks(15);
    if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
    SET_CURSOR_POSITION(pos_y,pos_x);
    SET_BACKGROUND_COLOR(127);
    SET_FONT_COLOR(15);
    printf("D%4d",i);
    pos_x = pos_x + 6;
    i++;
    if (i==40) noyau_exit();
  }
}

int main()
{
	serial_init(0, 115200);
	CLEAR_SCREEN(1);
    SET_CURSOR_POSITION(1,1);
    test_colors();
    CLEAR_SCREEN(1);
    SET_CURSOR_POSITION(1,1);
    puts("Test noyau");
    puts("Noyau preemptif");
    SET_CURSOR_POSITION(5,1);
    SAVE_CURSOR_POSITION();
	start(tachedefond);
  return(0);
}
