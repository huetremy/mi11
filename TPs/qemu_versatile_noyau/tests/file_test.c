#ifdef __arm__
#include "serialio.h"
#endif
#ifdef __x86_64__
#include <stdio.h>
#endif

#include "noyau_file.h"

void affic_all(void) {
  file_affiche();
  file_affiche_queue();
  puts("Appuyez sur une touche pour continuer...\n");
  getchar();
}

int main(int argc, char *argv[]) {
  file_init();
  file_ajoute(3);
  file_ajoute(5);
  file_ajoute(1);
  file_ajoute(0);
  file_ajoute(2);

  printf("File initiale : \n");
  affic_all();

  printf("--------------\n");
  printf("suivant() = %d\n", file_suivant());
  affic_all();

  printf("--------------\n");
  printf("retire(0)\n");
  file_retire(0);
  affic_all();

  printf("--------------\n");
  printf("retire(3)\n");
  file_retire(3);
  affic_all();

  printf("--------------\n");
  printf("ajoute(6)\n");
  file_ajoute(6);
  affic_all();

  return (0);
}
