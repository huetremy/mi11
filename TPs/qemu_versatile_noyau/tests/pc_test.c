/*----------------------------------------------------------------------------*
 * fichier : noyau_test.c                                                     *
 * programme de test du noyaut                                                *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include <stdlib.h>

#include "serialio.h"
#include "noyau.h"
#include "TERMINAL.h"
#include "FIFO.h"
#include "SEM.h"

TACHE	tacheA(void);
TACHE	Prod(void);
TACHE	Cons(void);


#define MAX_CARA_LIGNE 80

uint16_t pos_x = 1;
uint16_t pos_y = 5;
FIFO f;
uint8_t mutex;
uint8_t semprod;
uint8_t semcons;

TACHE	tacheA(void)
{
  SET_CURSOR_POSITION(3,1);
  puts("------> EXEC tache A");
  mutex = s_cree(1);
  semprod = s_cree(0);
  semcons = s_cree(0);

  active(cree(Cons));
  active(cree(Prod));
  fin_tache();
}



TACHE Prod( void )
{
	uint16_t d = 0;
	long j;

	SET_CURSOR_POSITION(4,1);
	puts("------> DEBUT tache B\n");
	SAVE_CURSOR_POSITION();


	while(d < 100)
	{
		for (j=0; j<2500000L; j++);
		s_wait(mutex);
		if (!fifo_ajoute(&f, d))
		{
			if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
			SET_CURSOR_POSITION(pos_y,pos_x);
			SET_BACKGROUND_COLOR(33);
			SET_FONT_COLOR(236);
			printf(" Pdort");
			pos_x = pos_x + 6;
			s_signal(mutex);
			s_wait(semprod);
		}
		else
		{
			if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
			SET_CURSOR_POSITION(pos_y,pos_x);
			SET_BACKGROUND_COLOR(46);
			SET_FONT_COLOR(236);
			printf(" P%4d", d);
			pos_x = pos_x + 6;
			d++;
			if (f.fifo_taille == 1)
			{
				if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
				SET_CURSOR_POSITION(pos_y,pos_x);
				SET_BACKGROUND_COLOR(75);
				SET_FONT_COLOR(236);
				printf(" PrevC");
				pos_x= pos_x + 6;
				if (s_getval(semcons) < 0)
					s_signal(semcons);
			}
			s_signal(mutex);
		}
	}
	s_wait(mutex);
	printf(" Fin du décompte");
	pos_x= pos_x + 16;
	s_signal(mutex);
	fin_tache();
}

TACHE Cons( void )
{
	uint8_t d;
	long int j;

	while(1)
	{
		for (j=0; j<10000000L; j++);
		for (j=0; j<5; j++)
		{
			s_wait(mutex);
			if (!fifo_retire(&f, (uint8_t *) &d))
			{
				if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
				SET_CURSOR_POSITION(pos_y,pos_x);
				SET_BACKGROUND_COLOR(220);
				SET_FONT_COLOR(236);
				printf(" Cdort");
				pos_x = pos_x + 6;
				s_signal(mutex);
				s_wait(semcons);
			}
			else
			{
				if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
				SET_CURSOR_POSITION(pos_y,pos_x);
				SET_BACKGROUND_COLOR(196);
				SET_FONT_COLOR(236);
				printf(" C%4d", d);
				pos_x = pos_x + 6;
				if (f.fifo_taille == TAILLE_FIFO - 1)
				{
					if (pos_x > MAX_CARA_LIGNE) {pos_x = 1; pos_y = pos_y+1;}
					SET_CURSOR_POSITION(pos_y,pos_x);
					SET_BACKGROUND_COLOR(208);
					SET_FONT_COLOR(236);
					printf(" CrevP");
					pos_x = pos_x + 6;
					if (s_getval(semprod) < 0)
						s_signal(semprod);
				}
				s_signal(mutex);
			}
		}
	}
}

int main()
{
  serial_init(0, 115200);
  CLEAR_SCREEN(1);
  SET_CURSOR_POSITION(1,1);
  test_colors();
  CLEAR_SCREEN(1);
  SET_CURSOR_POSITION(1,1);
  puts("Test noyau");
  puts("Noyau preemptif");
  SET_CURSOR_POSITION(5,1)
  SAVE_CURSOR_POSITION();
  start(tacheA);
  return(0);
}