let
  sources = import ./nix/sources.nix;
  rust = import ./nix/rust.nix { inherit sources; };
  pkgs = import sources.nixpkgs {} ;
  unstable = import sources.nixpkgs-unstable {};
in
pkgs.mkShell {
  buildInputs = [
    pkgs.clang
    pkgs.cppcheck
    pkgs.gcc-arm-embedded
    pkgs.gdb-multitarget
    pkgs.gnumake
    pkgs.linuxHeaders
    unstable.openocd
    rust
    pkgs.qemu
  ];
}
