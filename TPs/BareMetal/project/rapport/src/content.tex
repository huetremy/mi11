\section{Sujet et objectifs du TP}

L'objectif du TP est de construire de zéro une application sur un processeur STM32H7 embarqué sur la carte de développement \enquote{stm32h747i-disco}.
Le programme aura pour objectif de faire clignoter une LED en utilisant un timer présent dans le c\oe ur avec le mécanisme d'interruptions.

Pour ce TP, nous n'utiliserons qu'un seul des deux c\oe urs présents dans le SoC, le c\oe ur principal, de la famille Cortex-M7.

Ce TP se fait \enquote{from scratch} : les seuls outils utilisés sont les outils de compilation croisée (\texttt{gcc} et \texttt{binutils} pour \texttt{arm-none-eabi}).
Ainsi, nous n'utiliserons aucun script d'édition de liens ni de script de démarrage extérieur.

Le TP est réalisé en plusieurs étapes :
\begin{itemize}
    \item prise en main et clignotement de la LED avec une boucle d'attente;
    \item utilisation d'un timer, que l'on surveille dans une boucle;
    \item utilisation d'un timer avec interruption.
\end{itemize}

En parallèle, il a fallu créer les scripts d'édition de lien et de démarrage du programme, ainsi que faire le nécessaire pour que la cible démarre sur notre programme.
L'ensemble du travail réalisé est expliqué à la section \ref{sec:realisation} et le code source est disponible en annexes.

\section[Langages \& outils]{Langages \& outils utilisés}

\subsection{Programmation}

La cible, sans la présence d'un système d'exploitation, ne sait exécuter que du code objet préalablement téléversée sur celle-ci.
Pour réaliser ce code il est donc possible d'utiliser un ou plusieurs langages permettant une compilation sous forme de code objet, notamment l'assembleur, le C/C++ et le Rust.

J'ai décidé pour ce TP d'utiliser autant que possible le langage Rust, qui est un langage moderne, sûr et adapté à la programmation système ou embarquée.
Ce langage assure par de nombreuses vérification à la compilation ainsi qu'une construction plus restreinte que le C/C++ (par exemple l'absence de valeur ou de référence nulle\footnote{ce que Tony \textsc{Hoare} appellera son \enquote{erreur à un milliard de dollars} : \url{https://www.infoq.com/presentations/Null-References-The-Billion-Dollar-Mistake-Tony-Hoare/}}).
Une fois compilé, le programme atteint des performances similaires à celles du C ou du C++.
Pour ces nombreuses qualités, Rust sera amené dans les prochaines années à remplacer en partie ou totalement C et C++ dans de nombreux domaines d'application.

J'ai donc commencé le TP en partant du principe que seulement la partie \enquote{programme utilisateur} serait faite en Rust, et que les autres éléments du programme (notamment le boot) seraient effectués en Assembleur et C.
Finalement, à l'exception d'une \texttt{libc} minimale requise par Rust et écrite en C, tout le code est fait en Rust.
Du code assembleur ayant été utilisé pendant le TP (puis remplacé), j'expliquerai comment il était intégré tout au long du rapport.

\subsection{Chaîne de construction}

\subsubsection{Compilation}

La compilation des différentes sources est effectuée à partir de différents compilateurs croisés gérant la cible :
\begin{itemize}
    \item pour l'assembleur et le C, on utilisera les outils GNU \texttt{gcc} et \texttt{as}, compilés et configurés pour la compilation croisée vers Cortex-M7 et avec l'ABI float \texttt{softfp}
    \item pour Rust, c'est le compilateur \texttt{rustc} qui est utilisé pour la génération de fichiers objet.
          La cible spécifiée est \texttt{thumbv7em-none-eabi} (il existe également la cible \texttt{thumbv7em-none-eabihf} pour utiliser l'ABI float hardware, mais il faut que les codes C et Rust utilisent la même ABI pour être linkés).
\end{itemize}

\subsubsection{Linkage}

Tous les fichiers sources présents (en assembleur, C ou Rust) sont compilés séparément sous forme de fichiers objets \texttt{.o}.

C'est le linker GNU \texttt{ld} qui est ensuite utilisé pour lier tous ces objets et générer le ficher ELF exécutable.
Le script d'édition de lien est écrit à la main pour la cible (voir section \ref{sec:realisation}).

On peut ensuite utiliser \texttt{objcopy} pour générer une image mémoire pour la cible.
Comme tous les programmes ont été chargées à partir du débogueur, je n'ai pas utilisé cet outil.

\subsubsection{Déboguage}

Pour tester le code, j'ai effectué du \enquote{Debug On Chip} sur la cible.
Ceci est fait en utilisant \texttt{openocd} pour la liaison avec la sonde JTAG de la cible, et \texttt{gdb-multiarch} pour le déboguage.

Cette combinaison est adaptée au déboguage du code pour tous les langages que j'ai utilisés dans le TP.

\subsubsection{Orchestration}

L'utilisation de tous ces outils est orchestrée au sein d'une architecture de projet basée sur un \texttt{Makefile}.
Celle-ci sépare les différentes architectures et cibles au sein d'une même architecture (exemple : utilisation de la carte de TP et d'une STM32F3-DISCOVERY).
Cela permet en fonction des variables \texttt{ARCH} et \texttt{TARGET} utilisées par le \texttt{Makefile} de charger de la configuration (arguments pour les outils, code spécifique\dots) spécifiques à la cible.

Cette architecture et l'utilisation d'un \texttt{Makefile} ne faisant pas partie de l'UV MI11, elle ne sera pas expliquée dans ce rapport, mais tout de même présente en annexe.

\section{Réalisation du TP}
\label{sec:realisation}

\subsection{libc}

Comme C++, Rust s'attend à la présence d'une libc minimale pour fonctionner.
Celle-ci doit comprendre les fonctions \texttt{memcmp}, \texttt{memcpy}, \texttt{memmove} et \texttt{memset} déclarée dans le header \texttt{string.h}.

Comme le TP consistait à tout écrire de zéro sur la cible, j'ai implémenté ces fonctions à la main en C (on aurait également pu les faire en Rust ou en assembleur).
J'ai donc créé un header \texttt{string.h}, puis implémenté les fonctions dans des fichiers \texttt{.c} séparés.

Pour cette architecture, le nom des fonctions est préfixé par \texttt{\_\_aeabi\_}.
La fonction \texttt{\_\_aeabi\_memcpy} dispose de deux variantes assumant respectivement que les données sont alignées sur 4 et 8 octets.
Ma fonction de base étant générale en effectuant une copie octet par octet, je me suis contenté de l'appeler dans les variantes.

\subsection{Programme principal}

Le programme principal est représenté par la fonction \texttt{main} du fichier Rust \texttt{main.rs}.
Celui-ci définit sous la forme de constantes les différentes adresses des registres utilisés dans le programme.

Pour avoir une construction assez générique et facilement modifiable, sans, par manque de temps, créer des structures mappées en mémoire, j'ai créé des modules contenant des constantes internes (adresse de base et offset par exemple) et exportant des constantes sommes des deux.
Cette construction n'implique aucun surcoût, les symboles \texttt{const} étant remplacés par leur valeur à la compilation.
Un exemple est donné au programme~\ref{lst:mod} : les symboles marqués \texttt{pub} sont les seuls accessibles depuis l'extérieur du module.

\begin{listing}[ht]
    \inputminted[
        frame=lines,
        linenos,
        fontsize=\footnotesize,
        firstline=15,
        lastline=23
    ]{rust}{../src/main.rs}
    \caption{Exemple d'implémentation d'un module pour la définition des adresses}
    \label{lst:mod}
\end{listing}

La suite du programme écrit dans les différents registres du processeur afin d'obtenir le comportement attendu.

Il faut :
\begin{itemize}
    \item alimenter le port GPIO I et le timer TIM2 (par les registres \texttt{RCC\_AHB4ENR} et \texttt{RCC\_APB1LENR});
    \item configurer la broche correspondant à la LED en sortie (par le registre \texttt{GPIOI\_MODER});
    \item configurer le timer. Pour cela :
          \begin{itemize}
              \item on lui affecte une valeur (\texttt{64000000} pour une seconde sur le timer 64MHz) par le registre \texttt{TIM2\_ARR};
              \item on autorise l'interruption liée à l'événement de mise à jour (bit 0 du registre \texttt{TIM2\_DIER});
              \item on lance le timer en écrivant 1 dans le bit 0 du registre \texttt{TIM2\_CR1}.
          \end{itemize}
\end{itemize}

Toutes ces écritures dans des registres du processeur sont faites en utilisant la fonction Rust \texttt{core::ptr::write\_volatile}.
Une fois ces initialisations terminées et le compteur lancé, on boucle de manière infinie sur l'instruction assembleur \texttt{wfi} (Wait For Interrupt) afin de réduire l'activité du c\oe ur en attendant que le timer déclenche une interruption.

Pour que ce code principal fonctionne, il reste à réaliser les tâches suivantes:
\begin{itemize}
    \item implémenter le programme de démarrage de la cible, qui appellera la fonction \texttt{main};
    \item implémenter le gestionnaire d'exceptions afin d'allumer ou éteindre la LED lorsque le timer déclenche une interruption;
    \item implémenter un \enquote{panic handler} requis par Rust;
    \item créer le script d'édition de liens et la table des vecteurs d'interruption.
\end{itemize}

\subsection{Table des vecteurs}

\subsubsection{Objectif}

L'objectif de la table des vecteurs est double.
Elle permet en effet:

\begin{itemize}
    \item l'initialisation de la cible lors de son redémarrage et du reset
    \item la prise en compte de l'interruption déclenchée par TIM2.
\end{itemize}

Notre table des vecteurs devra donc contenir les adresses des fonctions à appeler dans pour les interruptions de reset et de TIM2.
Pour la valeur initiale du pointeur de pile, voir la section~\ref{ssec:ldscript}.

\subsubsection{Réalisation}

La création du vecteur d'exception est faite dans le fichier \texttt{arch/armv7em/vector.rs}.
Ce fichier définit les vecteurs d'interruption jusqu'à celui correspondant à l'interruption du timer.

Pour cela, on définit un \texttt{union} vector, pouvant contenir différentes variantes, toutes de 32 bits (voir programme~\ref{lst:unionvec}).
Ces variantes peuvent stocker un pointeur de fonction, retournant ou non (\texttt{handler} retourne, \texttt{reset} non), ou une valeur entière que l'on mettra à 0 pour signifier que l'emplacement est réservé ou non implémenté (deux variantes sont présentes pour des raisons de sémantique, mais cela ne fait aucune différence pour le programme).

\begin{listing}[ht]
    \inputminted[
        frame=lines,
        linenos,
        fontsize=\footnotesize,
        firstline=8,
        lastline=13
    ]{rust}{../arch/armv7em/vector.rs}
    \caption{Définition de l'union Vector}
    \label{lst:unionvec}
\end{listing}

En Rust, le type \texttt{usize} a la taille des pointeurs pour l'architecture vers laquelle on compile.
Pour toutes les architectures où la taille de pointeur de fonction est la même que la taille de pointeur vers une données (dont l'architecture que l'on utilise), on garantit que toutes les variantes de l'union auront la même taille.
Ici, chaque entrée fera 32 bits, ce qui correspond bien à la taille d'une entrée de la table des vecteurs.

On peut ensuite définir la table des vecteurs comme un tableau de taille fixe contenant les vecteurs.
Comme on ne souhaite gérer que les exceptions reset et TIM2, on ne fera un vecteur contenant que 44 entrées, et on mettra toutes les interruptions entre reset et TIM2 à \texttt{unimplemented: 0}.
Un extrait est montré au programme~\ref{lst:extraitvec}.

\begin{listing}
    \begin{minted}[
        frame=lines,
        linenos,
        fontsize=\footnotesize
    ]{rust}
    #[link_section = ".vector_table.exceptions"]
    #[no_mangle]
    pub static EXCEPTIONS: [Vector; 44] = [
        Vector{reset: _start}, // Reset
        Vector{unimplemented: 0}, // NMI
        Vector{unimplemented: 0}, // HardFault
        ...
        Vector{unimplemented: 0}, // TIM_CC
        Vector{handler: timer_handler}, // TIM2
    ];
    \end{minted}
    \caption{Extrait de la table des vecteurs}
    \label{lst:extraitvec}
\end{listing}

Les attributs au dessus de la définition du vecteur s'appliquent à celui-ci.
Ils définissent la section dans laquelle le placer lors de la compilation et interdisent le name mangling pour ce symbole, ce qui nous permettra de le récupérer facilement dans le script d'édition de liens.

Pour l'utilisation de ce vecteur, voir la sections suivante.

\subsection{Script d'édition de liens}
\label{ssec:ldscript}

Le script d'édition de liens permet la construction du programme résultant du code source.
Il spécifie notamment l'architecture de sortie, l'entrée du programme (utile pour le déboguage), les différentes zones mémoire utilisées et le placement des sections.

En plus, il permet l'utilisation de symboles globaux définis dans le programme, et peut aussi exporter des symboles qui pourront être utilisés par le programme.

Le script d'édition de liens définit deux zones mémoire :
\begin{itemize}
    \item \texttt{SRAM4} correspondant à une des zones de RAM du processeur.
          Cette zone est marquée \texttt{rw} et contiendra la section \texttt{.bss} et la pile.
    \item \texttt{FLASH} correspondant à la mémoire flash principale du processeur.
          Cette zones est marquée \texttt{rx} et contiendra la table des vecteurs et les sections de code \texttt{.text} et les données en lecture seule \texttt{.rodata}.
\end{itemize}

La section \texttt{.data} est un cas spécial : elle contient des données modifiables disposant d'une valeur à l'initialisation du programme.
Il faut donc que ces variables soient situées en RAM pour être modifiables par le programme, mais que leur valeur initiale soit stockées en flash pour être gardées en mémoire une fois la cible éteinte.

Pour cela, le placement de la section est tel que montré au programme~\ref{lst:ld.data}.
Ainsi, la section dispose de deux adresses:
\begin{itemize}
    \item une \textsl{LMA} (\textsl{Load Memory Address}) qui correspond à l'adresse de la section dans l'image mémoire (soit les données persistantes en flash);
    \item une \textsl{VMA} (\textsl{Virtual Memory Address}) qui correspond à l'adresse de la section lors de l'exécution du programme (en RAM).
\end{itemize}

\begin{listing}
    \inputminted[
        frame=lines,
        linenos,
        fontsize=\footnotesize,
        firstline=32,
        lastline=39
    ]{text}{../arch/armv7em/target/stm32h7i/linkerscript.x}
    \caption{Placement de la section \texttt{.data} dans le ldscript}
    \label{lst:ld.data}
\end{listing}

Les lignes 37 à 39 du~\ref{lst:ld.data} exportent des symboles correspondant respectivement à la \textsl{LMA}, la \textsl{VMA} et à la taille de la section.
Grâce à ces symboles, notre script de démarrage pourra copier le contenu des données dans la RAM au démarrage du programme.

De la même manière, le script exporte l'adresse et la taille de la section \textsc{.bss}, que notre script de démarrage initialisera à 0.

\subsection{Panic Handler}

Rust s'attend à la présence dans le programme d'un \enquote{panic handler}, fonction appelée lorsque la macro \texttt{panic!} est appelée.
Celle-ci est fournie en temps normal par la \texttt{libstd} de Rust, que nous n'utilisons par dans notre cas.

La seule contrainte pour cette fonction est qu'elle ne doit pas retourner.
Dans notre cas, on peut donc se contenter d'une boucle infinie comme dans le fichier \texttt{src/rust/panic.rs}.

\subsection{Script de démarrage}

Le script de démarrage définit la fonction \texttt{\_start}, point d'entrée du programme et fonction pointée par le vecteur d'interruption \texttt{reset}.
Cette fonction doit réaliser les actions suivantes:
\begin{itemize}
    \item copier des données de \texttt{.data} de la flash vers la RAM;
    \item initialiser les données de \texttt{.bss} à 0;
    \item appeler la fonction \texttt{main};
    \item faire quelque chose au retour (éventuel) de \texttt{main}.
\end{itemize}

La valeur du pointeur de pile étant définie dans la table des vecteurs, il n'y a pas besoin de code assembleur pour cette fonction de démarrage.
Elle peut donc être écrite directement en C, ou même en Rust.

\subsubsection{Sections \texttt{.bss} et \texttt{.data}}

Pour l'initialisation des sections \texttt{.bss} et \texttt{.data}, le programme utilise les symboles définis dans le script d'édition de liens.
Pour le script, ces symboles sont des pointeurs, là où nous souhaitons les utiliser en tant que valeurs.
Afin de régler ce problème, certaines manipulations doivent être effectuées, notamment l'utilisation de \texttt{transmute} afin de réinterpréter les symboles de taille des sections.

Une fois ces manipulations effectuées, le programme peut copier les données de \texttt{.data} de la flash vers la RAM en utilisant la fonction \texttt{core::ptr::copy\_nonoverlapping} et initialiser la section \texttt{.bss} avec la fonction \texttt{core::ptr::write\_bytes}.
Ces fonctions font elles-mêmes appel aux fonctions \texttt{memcpy} et \texttt{memset} que nous avions créées précédemment.
Elles font partie de la \texttt{libcore} de Rust, qui est indépendante de la cible et est donc présente quelle qu'elle soit (sous condition de l'implémentation minimale de la libc requise).

\subsubsection{Appel et retour de \texttt{main}}

La fonction main est déclarée comme étant un symbole Rust externe dans le fichier \texttt{arch/armv7em/start.rs} et peut donc être appelée par le programme.
Contrairement au C, il est possible de spécifier en Rust qu'une fonction ne retourne jamais.
C'est le cas pour la fonction \texttt{main}, il n'y a donc pas à se soucier de son retour dans le script de démarrage.

\section{Conclusion}

L'ensemble du code réalisé permet la création sans apport de code externe d'un programme faisant clignoter une diode de la carte de développement.
Il s'agit d'un programme minimal, ne prenant en compte que les besoins directs de celui-ci.
Il manque par exemple beaucoup de gestionnaires d'exception, notamment celui du HardFault.

\subsection{À propos de l'utilisation de Rust}

Le langage Rust est parfaitement adapté à la création de tels programmes.
En effet, si le Rust \enquote{commun} préfère l'utilisation d'abstractions haut niveau pour la simplicité et la sûreté du programme, il est tout de même possible de réaliser en Rust la quasi totalité de ce que l'on peut faire en C (accès direct à la mémoire, \enquote{memory mapped structs}, casts\dots).

On notera néanmoins que l'absence de librairie standard et la nécessiter de déclarer la quasi totalité du code \enquote{unsafe} réduit l'intérêt de l'utilisation de ce langage pour ce type de programme, à moins d'utiliser un \enquote{crate} (bibliothèque Rust) pour la cible.
Ceux-ci permettent via un niveau d'abstraction des interactions en \enquote{safe Rust} avec le matériel (les parties de code \enquote{unsafe} sont dans le \enquote{crate}, qui propose une API \enquote{safe}).

\subsection{Pour aller plus loin}

Pour aller plus loin dans la réalisation de ce TP, plusieurs pistes pourraient être explorées:
\begin{itemize}
    \item le support de plus d'architectures / cibles grâce à l'architecture Makefile créée;
    \item le développement d'abstractions en Rust pour l'utilisation des périphériques (GPIO, timers\dots).
          Celui-ci pourrait passer par la création de \enquote{memory mapped structs} pour la simplification des accès, où même l'écriture d'un \enquote{crate} de plus haut niveau.
    \item Une meilleure gestion des interruptions, en prenant notamment en compte les interruptions non masquables.
\end{itemize}

\clearpage

\section*{Annexe - Installation de Rust}

Installer rustup (gestionnaire Rust)

\begin{verbatim}
$ curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
\end{verbatim}

Installer la toolchain \enquote{nightly} de Rust (l'assembleur inline n'est pas encore intégré à la version stable):

\begin{verbatim}
$ rustup toolchain install nightly
\end{verbatim}

Ajouter l'architecture cible:

\begin{verbatim}
$ rustup target add thumbv7em-none-eabi --toolchain nightly
\end{verbatim}

Pour que la version de Rust utilisée soit bien nightly, deux options:

\begin{itemize}
    \item soit changer la toolchain par défaut:
          \begin{verbatim}
$ rustup override set nightly
\end{verbatim}
    \item soit modifier le Makefile pour utiliser la toolchain nightly.
          Il faut alors changer la ligne 55 du Makefile par
          \begin{verbatim}
RUST=rustup run nightly rustc
    \end{verbatim}
\end{itemize}
