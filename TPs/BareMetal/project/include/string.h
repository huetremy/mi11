//
// Created by remy on 3/11/21.
//

#ifndef PROJECT_STRING_H
#define PROJECT_STRING_H

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

int __aeabi_memcmp(const void *, const void *, size_t);

void *__aeabi_memcpy(void *__restrict, const void *__restrict, size_t);

void *__aeabi_memmove(void *, const void *, size_t);

void *__aeabi_memset(void *, int, size_t);

#ifdef __cplusplus
}
#endif

#endif // PROJECT_STRING_H
