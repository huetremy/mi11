#![no_std]

extern "Rust" {
    fn main() -> !;
}

extern "C" {
    static data_lma: u8;
    static mut data_vma: u8;
    static data_size: usize;

    static mut bss_start: u8;
    static bss_size: usize;
}

#[no_mangle]
pub extern "C" fn _start() -> ! {

    // Folowing code is unsafe because:
    // - It reads extern static variables
    // - It operates memory transmutations to reinterpret data_size and bss_size
    //      as values and not pointers
    // - It writes to memory using raw pointers dereference 
    //      (functions copy_nonoverlapping & write_bytes)
    unsafe {
        // Copy data from Load Memory Address to Virtual Memory Address
        let lma = &data_lma as *const u8;
        let vma = &mut data_vma as *mut u8;
        let size = core::mem::transmute::<&usize, usize>(&data_size);
        core::ptr::copy_nonoverlapping(lma, vma, size);

        // Set all bss values to 0
        let start = &mut bss_start as *mut u8;
        let size = core::mem::transmute::<&usize, usize>(&bss_size);
        core::ptr::write_bytes(start, 0, size);
        main();
    }
}
