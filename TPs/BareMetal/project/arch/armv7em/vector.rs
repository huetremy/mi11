#![no_std]

extern "C" {
    fn _start() -> !;
    fn timer_handler();
}

pub union Vector {
    reserved: usize,
    unimplemented: usize,
    handler: unsafe extern "C" fn(),
    reset: unsafe extern "C" fn() -> !,
}

#[link_section = ".vector_table.exceptions"]
#[no_mangle]
pub static EXCEPTIONS: [Vector; 44] = [
    Vector{reset: _start}, // Reset
    Vector{unimplemented: 0}, // NMI
    Vector{unimplemented: 0}, // HardFault
    Vector{unimplemented: 0}, // MemManage
    Vector{unimplemented: 0}, // BusFault
    Vector{unimplemented: 0}, // UsageFault
    Vector{reserved: 0}, // 1C
    Vector{reserved: 0}, // 20
    Vector{reserved: 0}, // 24
    Vector{reserved: 0}, // 28
    Vector{unimplemented: 0}, // SVCall
    Vector{unimplemented: 0}, // DebugMonitor
    Vector{reserved: 0}, // 34
    Vector{unimplemented: 0}, // PendSV
    Vector{unimplemented: 0}, // Systick
    Vector{unimplemented: 0}, // WWDG1 / 2
    Vector{unimplemented: 0}, // PVD_PWM
    Vector{unimplemented: 0}, // RTC_TAMP
    Vector{unimplemented: 0}, // RTC_WKUP
    Vector{unimplemented: 0}, // FLASH
    Vector{unimplemented: 0}, // RCC
    Vector{unimplemented: 0}, // EXTI0
    Vector{unimplemented: 0}, // EXTI1
    Vector{unimplemented: 0}, // EXTI2
    Vector{unimplemented: 0}, // EXTI3
    Vector{unimplemented: 0}, // EXTI4
    Vector{unimplemented: 0}, // DMA_STR0
    Vector{unimplemented: 0}, // DMA_STR1
    Vector{unimplemented: 0}, // DMA_STR2
    Vector{unimplemented: 0}, // DMA_STR3
    Vector{unimplemented: 0}, // DMA_STR4
    Vector{unimplemented: 0}, // DMA_STR5
    Vector{unimplemented: 0}, // DMA_STR6
    Vector{unimplemented: 0}, // ADC1_2
    Vector{unimplemented: 0}, // FDCAN1_IT0
    Vector{unimplemented: 0}, // FDCAN2_IT0
    Vector{unimplemented: 0}, // FDCAN1_IT1
    Vector{unimplemented: 0}, // FDCAN2_IT1
    Vector{unimplemented: 0}, // EXTI9_5
    Vector{unimplemented: 0}, // TIM1_BRK
    Vector{unimplemented: 0}, // TIM1_UP
    Vector{unimplemented: 0}, // TIM1_TRG_COM
    Vector{unimplemented: 0}, // TIM_CC
    Vector{handler: timer_handler}, // TIM2
];
