OUTPUT_FORMAT("elf32-littlearm", "elf32-bigarm", "elf32-littlearm")
OUTPUT_ARCH(arm)
ENTRY(_start)

MEMORY {
    SRAM : ORIGIN = 0x20000000, LENGTH = 40k
}

SECTIONS {
    . = ORIGIN(SRAM);
    .text ALIGN(4) : {
        *(.text)
        *(.text.*)
    }

    .rodata ALIGN(4) : {
        *(.rodata)
    }

    .data ALIGN(4) : {
        *(.data)
    }

    bss_start = .;
    .bss ALIGN(4) : {
        *(COMMON)
        *(.bss)
    }
    bss_end = .;

    stack = ORIGIN(SRAM) + LENGTH(SRAM);

    /DISCARD/ :
    {
        *(.ARM.exidx .ARM.exidx.*);
    }
}
