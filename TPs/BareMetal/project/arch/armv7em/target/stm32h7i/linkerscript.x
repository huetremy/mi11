OUTPUT_FORMAT("elf32-littlearm", "elf32-bigarm", "elf32-littlearm")
OUTPUT_ARCH(arm)
ENTRY(_start)

MEMORY {
    SRAM4(rw) : ORIGIN = 0x38000000, LENGTH = 64k
    FLASH(rx) : ORIGIN = 0x08000000, LENGTH = 256k
}

EXTERN(EXCEPTIONS);

SECTIONS {

    .vector_table ORIGIN(FLASH) :
    {
        /* Initial SP value */
        LONG(ORIGIN(SRAM4) + LENGTH(SRAM4));

        /* Exception vectors */
        KEEP(*(.vector_table.exceptions));
    } > FLASH

    .text ALIGN(4) : {
        *(.text)
        *(.text.*)
    } > FLASH

    .rodata ALIGN(4) : {
        *(.rodata)
    } > FLASH

    .data ORIGIN(SRAM4) : {
        *(.data)
        *(.data.*)
    } > SRAM4 AT> FLASH

    data_lma = LOADADDR(.data);
    data_vma = ADDR(.data);
    data_size = SIZEOF(.data);

    .bss ALIGN(4) : {
        *(.bss)
        *(.bss.*)
    } > SRAM4

    bss_start = ADDR(.bss);
    bss_size = SIZEOF(.bss);

    /DISCARD/ :
    {
        *(.ARM.exidx .ARM.exidx.*);
    }
}
