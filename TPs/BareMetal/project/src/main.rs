#![no_std]
#![feature(asm)]

use core::ptr;

mod rcc {
    const RCC_BASE: usize = 0x58024400;
    const AHB4ENR_OFFSET: usize = 0x0E0;
    const APB1LENR_OFFSET: usize = 0x0E8;

    pub const RCC_AHB4ENR: usize = RCC_BASE + AHB4ENR_OFFSET;
    pub const RCC_APB1LENR: usize = RCC_BASE + APB1LENR_OFFSET;
}

mod gpio {
    const GPIOI_BASE: usize = 0x58022000;

    const MODER_OFFSET: usize = 0x00;
    const BSRR_OFFSET: usize = 0x18;

    pub const GPIOI_MODER: usize = GPIOI_BASE + MODER_OFFSET;
    pub const GPIOI_BSRR: usize = GPIOI_BASE + BSRR_OFFSET;
}

mod timers {
    const TIM2_BASE: usize = 0x40000000;

    const CR1_OFFSET: usize = 0x00;
    const DIER_OFFSET: usize = 0x0C;
    const SR_OFFSET: usize = 0x10;
    // const CNT_OFFSET: usize = 0x24;
    // const PSC_OFFSET: usize = 0x28;
    const ARR_OFFSET: usize = 0x2C;

    pub const TIM2_CR1: usize = TIM2_BASE + CR1_OFFSET;
    pub const TIM2_SR: usize = TIM2_BASE + SR_OFFSET;
    pub const TIM2_DIER: usize = TIM2_BASE + DIER_OFFSET;
    // pub const TIM2_CNT: usize = TIM2_BASE + CNT_OFFSET;
    // pub const TIM2_PSC: usize = TIM2_BASE + PSC_OFFSET;
    pub const TIM2_ARR: usize = TIM2_BASE + ARR_OFFSET;
}

mod nvic {
    const NVIC_BASE: usize = 0xE000E100;

    pub const NVIC_ISER0: usize = NVIC_BASE;
}

const TIME: u32 = 64_000_000;

static mut LED_ON: bool = false;

fn led(state: bool) {
    unsafe {
        match state {
            true => ptr::write_volatile(gpio::GPIOI_BSRR as *mut u32, 1 << 12),
            false => ptr::write_volatile(gpio::GPIOI_BSRR as *mut u32, 1 << 28),
        }
    }
}

#[no_mangle]
pub extern "C" fn timer_handler() {
    unsafe {
        match LED_ON {
            true => {
                LED_ON = false;
                led(LED_ON);
            }
            false => {
                LED_ON = true;
                led(LED_ON);
            }
        }
        // Reset SR
        ptr::write_volatile(timers::TIM2_SR as *mut u16, 0);
    }
}

#[no_mangle]
pub fn main() -> ! {
    unsafe {
        // Enable GPIOI
        let mut rcc_ahb4enr_val = ptr::read_volatile(rcc::RCC_AHB4ENR as *const u32);
        rcc_ahb4enr_val |= 1 << 8;
        ptr::write_volatile(rcc::RCC_AHB4ENR as *mut u32, rcc_ahb4enr_val);

        // Set led1 as output
        let mut gpioi_moder_val = ptr::read_volatile(gpio::GPIOI_MODER as *const u32);
        gpioi_moder_val &= 0 << 25;
        gpioi_moder_val |= 1 << 24;
        ptr::write_volatile(gpio::GPIOI_MODER as *mut u32, gpioi_moder_val);

        // Enable TIM2 interrupt
        ptr::write_volatile(nvic::NVIC_ISER0 as *mut u32, 1 << 28); // Tout est à 0 de base

        // Enable TIM2
        let mut rcc_apb1lenr_val = ptr::read_volatile(rcc::RCC_APB1LENR as *const u32);
        rcc_apb1lenr_val |= 1 << 0;
        ptr::write_volatile(rcc::RCC_APB1LENR as *mut u32, rcc_apb1lenr_val);

        // Configure timer values
        ptr::write_volatile(timers::TIM2_ARR as *mut u32, TIME);

        // Configure interrupt
        ptr::write_volatile(timers::TIM2_DIER as *mut u16, 1 << 0);

        // Launch timer
        ptr::write_volatile(timers::TIM2_CR1 as *mut u16, 1 << 0); // reset is 0
    }

    loop {
        unsafe { asm!("wfi"); }
    }
}
