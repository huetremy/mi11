//
// Created by remy on 3/11/21.
//

#include <string.h>

void *__aeabi_memcpy(void *restrict dstptr, const void *restrict srcptr, size_t size) {
    unsigned char *dst = (unsigned char *) dstptr;
    const unsigned char *src = (const unsigned char *) srcptr;

    for (size_t i = 0; i < size; ++i) {
        dst[i] = src[i];
    }

    return dstptr;
}

void *__aeabi_memcpy4(void *restrict dstptr, const void *restrict srcptr, size_t size) {
    return __aeabi_memcpy(dstptr, srcptr, size);
}

void *__aeabi_memcpy8(void *restrict dstptr, const void *restrict srcptr, size_t size) {
    return __aeabi_memcpy(dstptr, srcptr, size);
}