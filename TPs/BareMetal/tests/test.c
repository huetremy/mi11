// Objectif : voir ce que donne ce boit de codez compilé

/* Variables et sections */
int a;                   // Common => potentiellement globale au programme
static int c;            // .bss => non initialisée
int b = 1;               // .data
const char *s = "Salut"; // *s dans .data (pointeur), "Salut" dans .rodata

// Code dans .text
int main(void)
{
    while (1)
    {
        c = 42;
        a = a + b + c + s[1] + f();
    }
}
