class A {
public:
  explicit A(int a) : a(a){};
  int getA() const { return a; }

private:
  int a;
};

int main() {
  A a(5);
  return a.getA();
}
