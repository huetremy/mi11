---
title: MI11 TP Linux 1
author: Rémy Huet
date: 17 mai 2021

geometry:
  - left=20mm
  - right=20mm
  - top=30mm
  - bottom=30mm
header-includes:
  - \usepackage{fvextra}
  - \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
---

# MI11 TP Linux 1

## Introduction

### Question 0.1

Le processeur est un processeur Texas Instrument (TI) AM3359.
Il est basé sur l'architecture Cortex-A8 (ARMv7) et est cadencé à 720MHz.

La cate dispose de 512Mo de SDRAM DDR3.

### Question 0.2

Le stockage du noyau et du système de fichiers peut se faire :

- en flash;
- par USB;
- sur une carte mémoire;
- via ethernet.

## Ex1 : prise en main de l'environement Yocto

### Question 1.1

les dossiers `opt/mi11/poky/build/conf`, `/opt/mi11/meta-devkit8600` et `/opt/mi11/meta-mi11` contiennent les fichiers de configuration pour la compilation du linux embarqué.

### Question 1.2

Les changements sont données sous forme de diff.

Dans `/opt/mi11/poky/build/conf` (configuration de poky) :

local.conf:

```diff
20c20,21
< MACHINE ?= "qemuarm"
---
> MACHINE ?= "devkit8600"
> #MACHINE ?= "qemuarm"
```

bblayers.conf:

```diff
11a12,14
>   /opt/mi11/meta-devkit8600 \
>   /opt/mi11/meta-versatilepb \
>   /opt/mi11/meta-mi11 \
```

### Question 1.3

On a généré le système de fichiers basique avec `bitbake core-image-base`.

```
WARNING: Host distribution "ubuntu-18.04" has not been validated with this version of the build system;
  you may possibly experience unexpected failures. It is recommended that you use a tested distribution.
Parsing recipes: 100% |#################################################################################
  #######################################| Time: 0:00:44
Parsing of 847 .bb files complete (0 cached, 847 parsed). 1355 targets, 80 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies

Build Configuration:
BB_VERSION        = "1.34.0"
BUILD_SYS         = "x86_64-linux"
NATIVELSBSTRING   = "ubuntu-18.04"
TARGET_SYS        = "arm-poky-linux-gnueabi"
MACHINE           = "devkit8600"
DISTRO            = "poky"
DISTRO_VERSION    = "2.3.4"
TUNE_FEATURES     = "arm armv7a vfp thumb neon callconvention-hard"
TARGET_FPU        = "hard"
meta
meta-poky
meta-yocto-bsp
meta-devkit8600
meta-versatilepb
meta-mi11         = "<unknown>:<unknown>"

Initialising tasks: 100% |###############################################################################
  ######################################| Time: 0:00:06
NOTE: Executing SetScene Tasks
NOTE: Executing RunQueue Tasks
NOTE: Tasks Summary: Attempted 3681 tasks of which 2912 didn't need to be rerun and all succeeded.

Summary: There was 1 WARNING message shown.
```

Dans `/opt/mi11/poky/build/tmp/deploy/images` on trouve notamment le rootfs du linux généré.
On y trouve les répertoires habituels d'un système de fichiers Linux.

Le système de fichiers fait 20Mo. À titre de comparaison, un image disque Ubuntu pèse 2.7Go.
Le linux généré est beaucoup plus léger car il embarque moins de fonctionnalités que pour un PC, il est également généré pour une cible spécifique et pas une architecture.

`ls` :

```
total 12M
[...] core-image-base-devkit8600-20210517132630.rootfs.manifest
[...] core-image-base-devkit8600-20210517132630.rootfs.tar.bz2
[...] core-image-base-devkit8600-20210517132630.testdata.json
[...] core-image-base-devkit8600.manifest -> core-image-base-devkit8600-20210517132630.rootfs.manifest
[...] core-image-base-devkit8600.tar.bz2 -> core-image-base-devkit8600-20210517132630.rootfs.tar.bz2
[...] core-image-base-devkit8600.testdata.json -> core-image-base-devkit8600-20210517132630.testdata.json
[...] modules--3.2.0-r0-devkit8600-20210515114907.tgz
[...] modules-devkit8600.tgz -> modules--3.2.0-r0-devkit8600-20210515114907.tgz
[...] uImage -> uImage--3.2.0-r0-devkit8600-20210515114907.bin
[...] uImage--3.2.0-r0-devkit8600-20210515114907.bin
[...] uImage-devkit8600.bin -> uImage--3.2.0-r0-devkit8600-20210515114907.bin
```

Sans compter les liens pour donner un nom simple à la dernière version compilées, on trouve 5 fichiers dans le répertoire :

- `core-image-base-devkit8600-20210517132630.rootfs.manifest` qui liste les paquets installés;
- `core-image-base-devkit8600-20210517132630.rootfs.tar.bz2` le système de fichiers sous forme de tarball compressée;
- `core-image-base-devkit8600-20210517132630.testdata.json` des données;
- `modules--3.2.0-r0-devkit8600-20210515114907.tgz` les modules noyau;
- `uImage--3.2.0-r0-devkit8600-20210515114907.bin` l'exécutable du noyau.

## Exercice 2 : Démarer le noyau

### Question 2.1

- La carte lance U-Boot en premier démarage
- U-Boot essaie de démarer par le réseau :
  - Il se place en client DHCP
  - Reçoit un adresse
  - Se connecte en TFTP à l'hôte
  - Elle essaie de récupérer le fichier uImage

```
U-Boot SPL 2011.09-svn (May 22 2012 - 11:19:00)
Texas Instruments Revision detection unimplemented
Booting from NAND...


U-Boot 2011.09-svn (May 22 2012 - 11:19:00)

I2C:   ready
DRAM:  512 MiB
WARNING: Caches not enabled
Did not find a recognized configuration, assuming General purpose EVM in Profile 0 with Daughter board
NAND:  HW ECC Hamming Code selected
512 MiB
MMC:   OMAP SD/MMC: 0
Net:   cpsw
Hit any key to stop autoboot:  0
Card did not respond to voltage select!
Booting from network ...
miiphy read id fail
link up on port 0, speed 100, full duplex
BOOTP broadcast 1
DHCP client bound to address 192.168.0.236
Using cpsw device
TFTP from server 192.168.0.1; our IP address is 192.168.0.236
Filename 'uImage'.
Load address: 0x82000000
Loading: *
TFTP error: 'File not found' (1)
Not retrying...
Wrong Image Format for bootm command
ERROR: can't get kernel image!
```

La carte ne parvient pas à récupérer le fichier : il n'est pas présent à l'endroit recherché.
La configuration de tftp-hpa est :

```
# /etc/default/tftpd-hpa

TFTP_USERNAME="tftp"
TFTP_DIRECTORY="/tftpboot"
TFTP_ADDRESS=":69"
TFTP_OPTIONS="--secure"
```

On copie le binaire `uImage--3.2.0-r0-devkit8600-X.bin` dans `/tftpboot` en le renommant `uImage`.
Il s'agit du noyau linux compilé pour la cible.

### Question 2.2

Après avoir placé le noyau dans `/tftpboot`, le noyau démarre sur la cible, jusqu'à une nouvelle erreur : il n'est pas possible de monter le système de fichier en nfs, le noyau s'arrête.

La configuration de ce qu iest exporté par l'hôte en nfs est :

```
# /etc/exports: the access control list for filesystems which may be exported
#               to NFS clients.  See exports(5).
#
# Example for NFSv2 and NFSv3:
# /srv/homes       hostname1(rw,sync,no_subtree_check) hostname2(ro,sync,no_subtree_check)
#
# Example for NFSv4:
# /srv/nfs4        gss/krb5i(rw,sync,fsid=0,crossmnt,no_subtree_check)
# /srv/nfs4/homes  gss/krb5i(rw,sync,no_subtree_check)
#
/tftpboot/      192.168.0.*(rw,sync,no_subtree_check,no_root_squash)
```

Le noyau cherche le filesystem dans `/tftpboot/rootfs/`.
On décompresse et extrait donc le système de fichier généré lors de la compilation dans `/tftpboot/rootfs/`.

### Question 2.3

Déjà fait

### Question 2.4

Fichier de logs à ajouter.

Processus de boot :

- Démarage de SPL et U-Boot depuis la FLASH
- U-Boot devient un client dhcp, se connecte à l'hôte en tftp et récupère le noyau.
- U-Boot démare le noyau
- Le noyau démarre et charge le système de fichiers en nfs.

La sortie terminal (sulr le port série) du boot complet est la suivante :

```
[   97.218707] �CCCCCCCC
U-Boot SPL 2011.09-svn (May 22 2012 - 11:19:00)
Texas Instruments Revision detection unimplemented
Booting from NAND...


U-Boot 2011.09-svn (May 22 2012 - 11:19:00)

I2C:   ready
DRAM:  512 MiB
WARNING: Caches not enabled
Did not find a recognized configuration, assuming General purpose EVM in Profile 0 with Daughter board
NAND:  HW ECC Hamming Code selected
512 MiB
MMC:   OMAP SD/MMC: 0
Net:   cpsw
Hit any key to stop autoboot:  3  2  1  0
Card did not respond to voltage select!
Booting from network ...
miiphy read id fail
link up on port 0, speed 100, full duplex
BOOTP broadcast 1
DHCP client bound to address 192.168.0.236
Using cpsw device
TFTP from server 192.168.0.1; our IP address is 192.168.0.236
Filename 'uImage'.
Load address: 0x82000000
Loading: * #################################################################
	 #################################################################
	 #####################################################
done
Bytes transferred = 2671792 (28c4b0 hex)
## Booting kernel from Legacy Image at 82000000 ...
   Image Name:   Linux-3.2.0
   Image Type:   ARM Linux Kernel Image (uncompressed)
   Data Size:    2671728 Bytes = 2.5 MiB
   Load Address: 80008000
   Entry Point:  80008000
   Verifying Checksum ... OK
   Loading Kernel Image ... OK
OK

Starting kernel ...

Uncompressing Linux... done, booting the kernel.
[    0.000000] Linux version 3.2.0 (oe-user@oe-host) (gcc version 6.4.0 (GCC) )
                 #1 Sat May 15 14:29:06 CEST 2021
[    0.000000] CPU: ARMv7 Processor [413fc082] revision 2 (ARMv7), cr=10c53c7d
[    0.000000] CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
[    0.000000] Machine: devkit8600
[    0.000000] Memory policy: ECC disabled, Data cache writeback
[    0.000000] AM335X ES1.0 (sgx neon )
[    0.000000] Built 1 zonelists in Zone order, mobility grouping on.  Total pages: 130048
[    0.000000] Kernel command line: console=ttyO0,115200n8 ip=dhcp noinitrd root=/dev/nfs rw rootwait
[    0.000000] PID hash table entries: 2048 (order: 1, 8192 bytes)
[    0.000000] Dentry cache hash table entries: 65536 (order: 6, 262144 bytes)
[    0.000000] Inode-cache hash table entries: 32768 (order: 5, 131072 bytes)
[    0.000000] Memory: 512MB = 512MB total
[    0.000000] Memory: 514088k/514088k available, 10200k reserved, 0K highmem
[    0.000000] Virtual kernel memory layout:
[    0.000000]     vector  : 0xffff0000 - 0xffff1000   (   4 kB)
[    0.000000]     fixmap  : 0xfff00000 - 0xfffe0000   ( 896 kB)
[    0.000000]     vmalloc : 0xe0800000 - 0xff000000   ( 488 MB)
[    0.000000]     lowmem  : 0xc0000000 - 0xe0000000   ( 512 MB)
[    0.000000]     modules : 0xbf000000 - 0xc0000000   (  16 MB)
[    0.000000]       .text : 0xc0008000 - 0xc04c5000   (4852 kB)
[    0.000000]       .init : 0xc04c5000 - 0xc04f9000   ( 208 kB)
[    0.000000]       .data : 0xc04fa000 - 0xc0545320   ( 301 kB)
[    0.000000]        .bss : 0xc0545344 - 0xc0587a8c   ( 266 kB)
[    0.000000] NR_IRQS:396
[    0.000000] IRQ: Found an INTC at 0xfa200000 (revision 5.0) with 128 interrupts
[    0.000000] Total of 128 interrupts on 1 active controller
[    0.000000] OMAP clockevent source: GPTIMER2 at 25000000 Hz
[    0.000000] OMAP clocksource: GPTIMER1 at 25000000 Hz
[    0.000000] I-pipe, 25.000 MHz clocksource
[    0.000000] sched_clock: 32 bits at 25MHz, resolution 40ns, wraps every 171798ms
[    0.000000] Interrupt pipeline (release #4)
[    0.000000] Console: colour dummy device 80x30
[    0.000508] Calibrating delay loop... 718.02 BogoMIPS (lpj=3590144)
[    0.119051] pid_max: default: 32768 minimum: 301
[    0.119182] Security Framework initialized
[    0.119248] Mount-cache hash table entries: 512
[    0.119670] CPU: Testing write buffer coherency: ok
[    0.120497] devtmpfs: initialized
[    0.125301] print_constraints: dummy:
[    0.125655] NET: Registered protocol family 16
[    0.127974] OMAP GPIO hardware version 0.1
[    0.130888] omap_mux_init: Add partition: #1: core, flags: 0
[    0.132906]  omap_i2c.1: alias fck already exists
[    0.133743]  da8xx_lcdc.0: alias fck already exists
[    0.135256]  omap_hsmmc.0: alias fck already exists
[    0.136720] _regulator_get: l3_main.0 supply vdd_core not found, using dummy regulator
[    0.136768] am335x_opp_update: physical regulator not present for core(-22)
[    0.137517]  omap2_mcspi.1: alias fck already exists
[    0.137749]  omap2_mcspi.2: alias fck already exists
[    0.138034]  edma.0: alias fck already exists
[    0.138055]  edma.0: alias fck already exists
[    0.138074]  edma.0: alias fck already exists
[    0.157147] bio: create slab <bio-0> at 0
[    0.159564] SCSI subsystem initialized
[    0.161269] usbcore: registered new interface driver usbfs
[    0.161599] usbcore: registered new interface driver hub
[    0.161822] usbcore: registered new device driver usb
[    0.162139] registerd cppi-dma Intr @ IRQ 17
[    0.162153] Cppi41 Init Done Qmgr-base(e087a000) dma-base(e0878000)
[    0.162164] Cppi41 Init Done
[    0.162210] musb-ti81xx musb-ti81xx: musb0, board_mode=0x13, plat_mode=0x3
[    0.162533] musb-ti81xx musb-ti81xx: musb1, board_mode=0x13, plat_mode=0x1
[    0.179072] omap_i2c omap_i2c.1: bus 1 rev2.4.0 at 100 kHz
[    0.180606] tps65910 1-002d: JTAGREVNUM 0x0
[    0.183039] print_constraints: VRTC:
[    0.184465] print_constraints: VIO: at 1500 mV
[    0.186767] print_constraints: VDD1: 600 <--> 1500 mV at 1262 mV normal
[    0.189054] print_constraints: VDD2: at 1137 mV
[    0.190051] print_constraints: VDD3: 5000 mV
[    0.191466] print_constraints: VDIG1: at 1800 mV
[    0.192878] print_constraints: VDIG2: at 1800 mV
[    0.194295] print_constraints: VPLL: at 1800 mV
[    0.195711] print_constraints: VDAC: at 1800 mV
[    0.197131] print_constraints: VAUX1: at 1800 mV
[    0.198549] print_constraints: VAUX2: at 3300 mV
[    0.199964] print_constraints: VAUX33: at 3300 mV
[    0.201378] print_constraints: VMMC: at 3300 mV
[    0.201870] tps65910 1-002d: No interrupt support, no core IRQ
[    0.203971] Switching to clocksource ipipe_tsc
[    0.223502] musb-hdrc: version 6.0, ?dma?, otg (peripheral+host)
[    0.223713] musb-hdrc musb-hdrc.0: dma type: dma-cppi41
[    0.224137] MUSB0 controller's USBSS revision = 4ea20800
[    0.225081] musb-hdrc musb-hdrc.0: USB OTG mode controller at e083c000 using DMA, IRQ 18
[    0.225259] musb-hdrc musb-hdrc.1: dma type: dma-cppi41
[    0.225577] MUSB1 controller's USBSS revision = 4ea20800
[    0.226034] musb-hdrc musb-hdrc.1: MUSB HDRC host driver
[    0.226141] musb-hdrc musb-hdrc.1: new USB bus registered, assigned bus number 1
[    0.226296] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002
[    0.226311] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    0.226324] usb usb1: Product: MUSB HDRC host driver
[    0.226335] usb usb1: Manufacturer: Linux 3.2.0 musb-hcd
[    0.226346] usb usb1: SerialNumber: musb-hdrc.1
[    0.227294] hub 1-0:1.0: USB hub found
[    0.227333] hub 1-0:1.0: 1 port detected
[    0.227922] musb-hdrc musb-hdrc.1: USB Host mode controller at e083e800 using DMA, IRQ 19
[    0.228388] NET: Registered protocol family 2
[    0.228613] IP route cache hash table entries: 4096 (order: 2, 16384 bytes)
[    0.228939] TCP established hash table entries: 16384 (order: 5, 131072 bytes)
[    0.229235] TCP bind hash table entries: 16384 (order: 4, 65536 bytes)
[    0.229412] TCP: Hash tables configured (established 16384 bind 16384)
[    0.229425] TCP reno registered
[    0.229437] UDP hash table entries: 256 (order: 0, 4096 bytes)
[    0.229460] UDP-Lite hash table entries: 256 (order: 0, 4096 bytes)
[    0.229662] NET: Registered protocol family 1
[    0.229943] RPC: Registered named UNIX socket transport module.
[    0.229957] RPC: Registered udp transport module.
[    0.229966] RPC: Registered tcp transport module.
[    0.229975] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    0.230200] NetWinder Floating Point Emulator V0.97 (double precision)
[    0.230430] omap-gpmc omap-gpmc: GPMC revision 6.0
[    0.230447] Registering NAND on CS0
[    0.231354] cpuidle-am33xx cpuidle-am33xx.0: failed to register driver
[    0.251660] VFS: Disk quotas dquot_6.5.2
[    0.251732] Dquot-cache hash table entries: 1024 (order 0, 4096 bytes)
[    0.252349] msgmni has been set to 1004
[    0.253174] io scheduler noop registered
[    0.253187] io scheduler deadline registered
[    0.253252] io scheduler cfq registered (default)
[    0.254913] Could not set LED4 to fully on
[    0.256159] da8xx_lcdc da8xx_lcdc.0: GLCD: Found AT043TN24 panel
[    0.263669] Console: switching to colour frame buffer device 60x34
[    0.268922] omap_uart.0: ttyO0 at MMIO 0x44e09000 (irq = 72) is a OMAP UART0
[    0.952540] console [ttyO0] enabled
[    0.957006] omap_uart.1: ttyO1 at MMIO 0x48022000 (irq = 73) is a OMAP UART1
[    0.965012] omap_uart.2: ttyO2 at MMIO 0x48024000 (irq = 74) is a OMAP UART2
[    0.972906] omap_uart.3: ttyO3 at MMIO 0x481a6000 (irq = 44) is a OMAP UART3
[    0.980793] omap_uart.4: ttyO4 at MMIO 0x481a8000 (irq = 45) is a OMAP UART4
[    0.988675] omap_uart.5: ttyO5 at MMIO 0x481aa000 (irq = 46) is a OMAP UART5
[    1.006961] brd: module loaded
[    1.015659] loop: module loaded
[    1.019146] i2c-core: driver [tsl2550] using legacy suspend method
[    1.025661] i2c-core: driver [tsl2550] using legacy resume method
[    1.034816] mtdoops: mtd device (mtddev=name/number) must be supplied
[    1.042159] omap2-nand driver initializing
[    1.046820] ONFI flash detected
[    1.050222] ONFI param page 0 valid
[    1.053866] NAND device: Manufacturer ID: 0xad, Chip ID: 0xdc (Hynix H27U4G8F2DTR-BC)
[    1.062431] Creating 8 MTD partitions on "omap2-nand.0":
[    1.068017] 0x000000000000-0x000000020000 : "SPL"
[    1.074690] 0x000000020000-0x000000040000 : "SPL.backup1"
[    1.081812] 0x000000040000-0x000000060000 : "SPL.backup2"
[    1.089020] 0x000000060000-0x000000080000 : "SPL.backup3"
[    1.096182] 0x000000080000-0x000000260000 : "U-Boot"
[    1.103527] 0x000000260000-0x000000280000 : "U-Boot Env"
[    1.110565] 0x000000280000-0x000000780000 : "Kernel"
[    1.119274] 0x000000780000-0x000020000000 : "File System"
[    1.335375] OneNAND driver initializing
[    1.384030] davinci_mdio davinci_mdio.0: davinci mdio revision 1.6
[    1.390489] davinci_mdio davinci_mdio.0: detected phy mask ffffffef
[    1.397975] davinci_mdio.0: probed
[    1.401530] davinci_mdio davinci_mdio.0: phy[4]: device 0:04, driver unknown
[    1.409326] usbcore: registered new interface driver cdc_ether
[    1.415604] usbcore: registered new interface driver cdc_eem
[    1.421665] usbcore: registered new interface driver dm9601
[    1.427542] cdc_ncm: 04-Aug-2011
[    1.431071] usbcore: registered new interface driver cdc_ncm
[    1.436991] Initializing USB Mass Storage driver...
[    1.442314] usbcore: registered new interface driver usb-storage
[    1.448596] USB Mass Storage support registered.
[    1.453500]  gadget: using random self ethernet address
[    1.458975]  gadget: using random host ethernet address
[    1.465087] usb0: MAC 5a:52:ad:8d:0b:55
[    1.469091] usb0: HOST MAC ea:6f:a3:bf:52:b7
[    1.473591]  gadget: Ethernet Gadget, version: Memorial Day 2008
[    1.479898]  gadget: g_ether ready
[    1.483468] musb-hdrc musb-hdrc.0: MUSB HDRC host driver
[    1.489080] musb-hdrc musb-hdrc.0: new USB bus registered, assigned bus number 2
[    1.496937] usb usb2: New USB device found, idVendor=1d6b, idProduct=0002
[    1.504038] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    1.511573] usb usb2: Product: MUSB HDRC host driver
[    1.516759] usb usb2: Manufacturer: Linux 3.2.0 musb-hcd
[    1.522297] usb usb2: SerialNumber: musb-hdrc.0
[    1.528050] hub 2-0:1.0: USB hub found
[    1.531992] hub 2-0:1.0: 1 port detected
[    1.537472] mousedev: PS/2 mouse device common for all mice
[    1.544539] input: ti-tsc as /devices/platform/omap/ti_tscadc/tsc/input/input0
[    1.553337] omap_rtc am33xx-rtc: rtc core: registered am33xx-rtc as rtc0
[    1.560628] i2c /dev entries driver
[    1.567010] OMAP Watchdog Timer Rev 0x01: initial timeout 60 sec
[    1.577773] usbcore: registered new interface driver usbhid
[    1.583596] usbhid: USB HID core driver
[    1.588362] tiadc tiadc: attached adc driver
[    1.592969] oprofile: hardware counters not available
[    1.598272] oprofile: using timer interrupt.
[    1.602758] nf_conntrack version 0.5.0 (8032 buckets, 32128 max)
[    1.609593] ip_tables: (C) 2000-2006 Netfilter Core Team
[    1.615293] TCP cubic registered
[    1.618670] NET: Registered protocol family 17
[    1.623351] Registering the dns_resolver key type
[    1.628356] VFP support v0.3: implementor 41 architecture 3 part 30 variant c rev 3
[    1.636389] ThumbEE CPU extension supported.
[    1.641125] mux: Failed to setup hwmod io irq -2
[    1.645962] Power Management for AM33XX family
[    1.650798] Trying to load am335x-pm-firmware.bin (60 secs timeout)
[    1.657477] Copied the M3 firmware to UMEM
[    1.661763] Could not deassert the reset for WKUP_M3
[    1.667007] Could not initialise WKUP_M3. Power management will be compromised
[    1.676307] clock: disabling unused clocks to save power
[    1.683587] Detected MACID=0:18:30:fe:a8:59
[    1.689061] cpsw: Detected MACID = 00:18:30:fe:a8:5a
[    1.696216] input: gpio-keys as /devices/platform/gpio-keys/input/input1
[    1.703924] omap_rtc am33xx-rtc: setting system clock to 2000-01-01 00:00:00 UTC (946684800)
[    1.717671] net eth0: CPSW phy found : id is : 0x4dd072
[    1.730619] PHY 0:01 not found
[    4.714550] PHY: 0:04 - Link is Up - 100/Full
[    4.734025] Sending DHCP requests ., OK
[    5.294485] IP-Config: Got DHCP answer from 192.168.0.1, my address is 192.168.0.151
[    5.305313] IP-Config: Complete:
[    5.308689]      device=eth0, addr=192.168.0.151, mask=255.255.255.0, gw=255.255.255.255,
[    5.316996]      host=192.168.0.151, domain=local, nis-domain=(none),
[    5.323715]      bootserver=192.168.0.1, rootserver=192.168.0.1, rootpath=/tftpboot/rootfs
[    5.343734] VFS: Mounted root (nfs filesystem) on device 0:14.
[    5.351021] devtmpfs: mounted
[    5.354553] Freeing init memory: 208K

INIT: version 2.88 booting

Starting udev
[    6.406747] udevd[604]: starting version 3.2.1
[    6.551807] udevd[605]: starting eudev-3.2.1
mount: mounting 192.168.0.1:/tftpboot/rootfs/ on / failed: Invalid argument
Mon May 17 13:29:40 UTC 2021


INIT: Entering runlevel: 5


Configuring network interfaces... ifup skipped for nfsroot interface eth0

run-parts: /etc/network/if-pre-up.d/nfsroot: exit status 1

Starting system message bus: dbus.

Starting Dropbear SSH server: dropbear.

Starting rpcbind daemon...done.

Starting syslogd/klogd: done

 * Starting Avahi mDNS/DNS-SD Daemon: avahi-daemon

   ...done.



Poky (Yocto Project Reference Distro) 2.3.4 devkit8600 /dev/ttyO0



devkit8600 login:
```

### Question 2.5

L'ip de la cible est 192.168.0.151.
On peut la trouver :

- dans les logs de démarrage ;
- dans les logs du serveur dhcp sur l'hôte ;
- en faisant un nmap du réseau de l'hôte.

### Question 2.6

`/proc/devices` affiche les périphériques d'entrée-sortie de caractères et les périphériques de blocs actuellement configurés.

```
Character devices:
  1 mem
  4 /dev/vc/0
  4 tty
  5 /dev/tty
  5 /dev/console
  5 /dev/ptmx
  7 vcs
 10 misc
 13 input
 29 fb
 89 i2c
 90 mtd
128 ptm
136 pts
180 usb
189 usb_device
252 ttyO
253 iio
254 rtc

Block devices:
  1 ramdisk
259 blkext
  7 loop
  8 sd
 31 mtdblock
 65 sd
 66 sd
 67 sd
 68 sd
 69 sd
 70 sd
 71 sd
128 sd
129 sd
130 sd
131 sd
132 sd
133 sd
134 sd
135 sd
179 mmc
```

La première section correspond aux périphériques de caractères et le second aux périphériques de blocs.

Le périphérique utilisé pour la communication série est `/dev/ttyO0`.
La ligne correspondante dans `/proc/devices` est la ligne `252 ttyO`. Le numéro en début de liste est le numéro de majeure.

### Question 2.7

La plupart des fichiers présents dans `/bin`, `/sbin` et `/usr/sbin` sont des liens symboliques vers busybox et non des exécutables.
Busybox est un exécutable qui combine beaucoup d'exécutables linux dans un seul programme.
Cela permet de regrouper les utilitaires souvent utilisés en un seul programme, ce qui en simplifie la compilation et le déploiement.

## Exercice 3 : ajout de paquets

Dans `/opt/mi11/poky/build` on lance `bitbake nano` pour installer nano.

### Question 3.1

Le dossier `/opt/mi11/poky/build/tmp/deploy/ipk` contient les paquets générés pour utiliser avec ipk.
Il est organisé par dossier en fonction de pour quoi le paquet est disponible (toutes architectures, architecture spécifique, cible spécifique).

Aisni, on retrouve le dossier `armv7ahf-neon` correspondant à l'architecture dans lequel se trouve nano.
Le kernel est spécifique à la cible et se trouve dans le dossier `devkit8600`.

### Question 3.2

Les paquets relatifs à nano sont tous situés dans `armv7ahf-neon` et contiennent notamment l'exécutable, les locales, la version de développement et la documentation.

On copie `nano_2.2.5-r3.0_armv7ahf-neon.ipk` dans `/tftpboot/rootfs/home/root`.
Le paquet est ensuite diponible sur la cible dans `/home/root`.

### Question 3.3

On essaie d'installer nano avec `opkg`.

Il manque une lib, l'erreur est :

```
Solver encountered 1 problem(s):
Problem 1/1:
  - nothing provides libncursesw5 >= 6.0+20161126 needed by nano-2.2.5-r3.0.armv7ahf-neon

Solution 1:
  - do not ask to install nano-2.2.5-r3.0.armv7ahf-neon
```

On pourrait le résoudre en n'installant pas nano.

### Question 3.4

Plutôt que de ne pas installer nano, on va configurer opkg pour qu'il récupère les paquets sur l'hôte avec leurs dépendances.
Pour cela, on modifie la configuration de opkg, on refait l'index des packages (`bitbake package-index`) sur l'hôte.
Sur la cible, on lance `opkg update` et `opkg install nano`.

opkg trouve alors les dépendances et nano et les télécharge et installe. Pour cela, il faut un serveur http sur la VM.

## Exercice 4 : compilation manuelle du noyau

### Question 4.1

D'après la documentation, on peut accéder aux LEDs en mode utilisateur en écrivant dans des fichiers :

```
root@DevKit8600:~# echo 1 > /sys/class/leds/user_led/brightness
root@DevKit8600:~# echo 0 > /sys/class/leds/user_led/brightness
```

On ne peut pas le faire sur la cible : la fonctionnalité n'est pas présente.

### Question 4.2

Le fichier `/opt/poky/2.3.4/armv7athf-neon/environment-setup-armv7ahf-neon-poky-linux-gnueabi` définit les variables d'environement pour la compilation croisée.

Le préfixe de la chaîne de compilation est `arm-poky-linux-gnueabi-`

### Question 4.3

La configuration par défaut pour l'architecture ARM est chargée par la variable d'environement ARCH.
Pour lister les configurations du noyau que l'on a, on peut utiliser la commande `make help`. La configuration de la carte est alors `devkit8600_defconfig`

On l'applique :

```
make devkit8600_defconfig
```

On lance menuconfig :

```
make menuconfig
```

### Qurstion 4.4

On va dans `Device Drivers -> LED Support` et on active `LED Support for GPIO connected LEDs` on peut soit l'ajouter au noyau directement, soit sous la forme de module. On le fera directement.

On lance la compilation avec `make uImage -j 2`.

### Question 4.5

Le noyau compilé est situé dans `/opt/mi11/linux-am33x/arch/arm/boot`

### Question 4.6

La version du noyau, dont sa date de compilation, est la première chose affichée au boot.

```
[    0.000000] Linux version 3.2.0 (mi11p21@mi11p21-VirtualBox) (gcc version 6.4.0 (GCC) )
                 #1 Mon May 17 17:53:34 CEST 2021
```

Les LEDs fonctionnent.

### Question 4.7

Utilisation des triggers pour les LEDs :

Dans `/sys/class/leds/user_led` :

```
cat trigger
```

Montre quel trigger est actif `none` par défaut et donne la liste des triggers disponibles.
On peut essayer les triggers, par exemple `heartbeat`, `timer`... et modifier leur configuration associée (en écrivant dans les fichiers).

## Exercice 5 : Compilation manuelle des modules noyau

### Question 5.1

On compile les modules avec la commande `make modules`.
