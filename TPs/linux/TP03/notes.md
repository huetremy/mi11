---
title: MI11 TP Xenomai 1
author: Rémy \textsc{Huet} (travail rendu seul)
date: 6 juin 2021

geometry:
  - left=20mm
  - right=20mm
  - top=30mm
  - bottom=30mm
header-includes:
  - \usepackage{fvextra}
  - \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
---


# MI11 Xenomai TP01

## Exercice 1 : Tâches

### Question 1.1

On adapte le code `HelloWorld.c` pour qu'il affiche en boucle toutes les secondes, et on le compile et l'exécute sur Xenomai.

Le fichier `/proc/xenomai/sched` qui donne la liste des tâches temps réel contient :

```
CPU  PID    CLASS  PRI      TIMEOUT   TIMEBASE   STAT       NAME
  0  0      idle    -1      -         master     R          ROOT
```

Il n'y a donc pas de tâche temps réel lancée : notre programme n'est pas une tâche temps réel.

### Question 1.2

Pour cross-compiler notre programme pour Xenomai avec temps réel, il faut fournir à GCC les chemins d'include et les bibliothèques avec lesquelles linker.
Cela se fait avec la commande suivante : 

```console
$ $CC -I/opt/poky/2.3.4/armv7athf-neon/sysroots/armv7ahf-neon-poky-linux-gnueabi/usr/include/xenomai 
    -lxenomai -lnative hello.c -o hello
```

Le programme avec la tâche temps réel Xenomai est donné ci-après :

```c
#include <stdio.h>
#include <unistd.h>

#include <sys/mman.h>

#include <native/task.h>
#include <rtdk.h>

// La tâche temps réel est une fonction
void hello_task() {
    while(1) {
        // On utilise les printf et sleep système
        printf("Hello world !\n");
	    sleep(1);
    }
}

int main() {
    // Descripteur de tâche
    RT_TASK task_desc;

    // On force la présence des pages mémoire du programme en RAM
    mlockall(MCL_CURRENT | MCL_FUTURE);

    // Création de la tâche temps réel nommée hello et avec une priorité de 99
    int err = rt_task_create(&task_desc, "hello", 0, 99, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create");
	    return 1;
    }

    // Démarrage de la tâche avec son adresse (fonction hello_task)
    rt_task_start(&task_desc, &hello_task, NULL);

    // La suite n'est jamais atteinte
    rt_task_join(&task_desc);

    rt_task_delete(&task_desc);

    return 0;
}
```

### Question 1.3

Le fihier `/proc/xenomai/stat` que l'on obtient est le suivant:

```
root@devkit8600-xenomai:~# cat /proc/xenomai/stat
CPU  PID    MSW        CSW        PF    STAT       %CPU  NAME
  0  0      0          118        0     00500080  100.0  ROOT
  0  993    1          1          0     00300380    0.0  hello
  0  0      0          22264      0     00000000    0.0  IRQ68: [timer]
```

Le code n'est pas vraiment temps réel car il utilise des appels système Linux qui ne prennent pas en compte le fait que la tâche est temps réel.

### Question 1.4

En replaçant le `sleep` Linux par le `rt_task_sleep` de Xenomai, le code devient:

```c
#include <stdio.h>
#include <unistd.h>

#include <sys/mman.h>

#include <native/task.h>
#include <rtdk.h>

void hello_task() {
    while(1) {
        printf("Hello world !\n");
        // Le sleep Xenomai est en ns
	    rt_task_sleep(1000000000);
    }
}

int main() {

    RT_TASK task_desc;

    mlockall(MCL_CURRENT | MCL_FUTURE);

    int err = rt_task_create(&task_desc, "hello", 0, 99, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create");
	    return 1;
    }

    rt_task_start(&task_desc, &hello_task, NULL);

    rt_task_join(&task_desc);

    rt_task_delete(&task_desc);

    return 0;
}
```

Le contenu du fichier de statistiques de Xenomai est alors :

```
CPU  PID    MSW        CSW        PF    STAT       %CPU  NAME
  0  0      0          152        0     00500080  100.0  ROOT
  0  998    17         34         0     00300184    0.0  hello
  0  0      0          23113      0     00000000    0.0  IRQ68: [timer]
```

### Question 1.5

En remplaçant l'appe à `printf` par son équivalent sous Xenomai, le code devient: 

```c
#include <stdio.h>
#include <unistd.h>

#include <sys/mman.h>

#include <native/task.h>
#include <rtdk.h>

void hello_task() {
    while(1) {
        // Utilisation du rt_printf
        rt_printf("Hello world !\n");
	    rt_task_sleep(1000000000);
    }
}

int main() {

    RT_TASK task_desc;

    mlockall(MCL_CURRENT | MCL_FUTURE);
    // Initialisation du rt_printf
    rt_print_auto_init(1);

    int err = rt_task_create(&task_desc, "hello", 0, 99, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create");
	return 1;
    }

    rt_task_start(&task_desc, &hello_task, NULL);

    rt_task_join(&task_desc);

    rt_task_delete(&task_desc);

    return 0;
}
```

Le fichier de statistiques de Xenomai donne alors le contenu suivant :

```
root@devkit8600-xenomai:~# cat /proc/xenomai/stat
CPU  PID    MSW        CSW        PF    STAT       %CPU  NAME
  0  0      0          86         0     00500080  100.0  ROOT
  0  977    0          78         0     00300184    0.0  hello
  0  0      0          21032      0     00000000    0.0  IRQ68: [timer]
```

Pour les trois programmes différents, on a :

- Avec le `printf` et le `sleep` 1 mode switch et 1 context switch : on quitte le mode temps réel au premier appel système, on y retourne jamais.
- Avec `printf` et `rt_task_sleep` 2 fois plus de context switch de de mode switch : on passe en mode normal à l'appel système printf, mais `rt_task_sleep` nous renvoie en mode temps réel.
- Avec `rt_printf` et `rt_task_sleep` aucun mode switch et 78 context switch : ici, on utilise pas d'appel système : on démarre la tâche en temps réel et elle y reste.

La seule bonnr solution pour avoir un programme temps réel est donc la dernière : il ne faut pas faire d'appel système Linux « normal » mais utiliser uniquement les fonctions temps réel fournies par Xenomai.


## Exercice 2

Programme lançant deux tâches affichant chacune une partie d'un message:

```c
#include <stdio.h>
#include <unistd.h>

#include <sys/mman.h>

#include <native/task.h>
#include <rtdk.h>

// La tâche hello affiche "Hello "
void hello_task() {
    rt_printf("Hello ");
}

// La tâche world affiche "world \n"
void world_task() {
    rt_printf("world \n");
}

int main() {
    // Flag d'erreur qui sera utilisé à chaque appel xenomai le nécessitant
    int err;

    // Descripteurs pour les tâches temps réel
    RT_TASK task_desc_hello;
    RT_TASK task_desc_world;

    // Lock des pages mémoire en RAM et init de rt_printf
    mlockall(MCL_CURRENT | MCL_FUTURE);
    rt_print_auto_init(1);

    // Création de la tâche hello
    err = rt_task_create(&task_desc_hello, "hello", 0, 99, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create for task hello");
	    return 1;
    }

    // Création de la tâche world
    err = rt_task_create(&task_desc_world, "world", 0, 99, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create for task world");
	    return 1;
    }

    // Lancement des tâches
    rt_task_start(&task_desc_hello, &hello_task, NULL);
    rt_task_start(&task_desc_world, &world_task, NULL);

    // On attend la terminaison des tâches (peu utile puisque main moins prioritaire)
    rt_task_join(&task_desc_hello);
    rt_task_join(&task_desc_world);

    // On supprime les tâches
    rt_task_delete(&task_desc_hello);
    rt_task_delete(&task_desc_world);

    // Fin du programme
    return 0;
}
```

Le programme affiche `Hello world`.

### Question 2.2

Les priorités n'ont aucune influence : même si la tâche `hello` a une priorité plus faible que la tache `world`, elle sera exécutée avant.
En effet, elle est plus prioritaire que `main` qui n'est pas une tâche temps réel : `main` doit donc attendre que `hello` soit finie avant de lancer `world`.

### Question 2.3

On initialise le sémaphore à 0 pour qu'il soit directement bloqué.
`main` utilisera `rt_sem_broadcast` pour libérer **toutes** les tâches en attente du sémaphore.

### Question 2.4

Le paramètre `mode` nous permet de déterminer su la tâche libérée en premier sera la première tâche arrivée (mode `FIFO`) ou la plus prioritaire (mode `PRIO`).
Dans notre cas, on choisit le mode `PRIO`.

### Question 2.5

```c
#include <stdio.h>
#include <unistd.h>

#include <sys/mman.h>

#include <native/task.h>
#include <native/sem.h>
#include <rtdk.h>

// Tâche hello
void hello_task(void* cookie) {
    // Récupération du sémaphore depuis les paramètres
    RT_SEM* sem = (RT_SEM*) cookie;
    // Attente sur le sémaphore, timeout infini
    rt_sem_p(sem, TM_INFINITE);
    // Affichage une fois le sémaphore passé
    rt_printf("Hello ");
}

void world_task(void* cookie) {
    // Récupération du sémaphore depuis les paramètres
    RT_SEM* sem = (RT_SEM*) cookie;
    // Attente sur le sémaphore, timeout infini
    rt_sem_p(sem, TM_INFINITE);
    // Affichage une fois le sémaphore passé
    rt_printf("world \n");
}

int main() {
    // Flag & descripteurs de tâches
    int err;
    RT_TASK task_desc_hello;
    RT_TASK task_desc_world;
    // Sémaphore Xenomai
    RT_SEM sem;

    // Création du sémaphore
    err = rt_sem_create(&sem, "sem", 0, S_PRIO);
    if(err != 0) {
        fprintf(stderr, "Error in rt_sem_create");
        return 1;
    }

    mlockall(MCL_CURRENT | MCL_FUTURE);
    rt_print_auto_init(1);

    // Création et lancement des tâches

    err = rt_task_create(&task_desc_hello, "hello", 0, 80, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create for task hello");
	    return 1;
    }

    err = rt_task_create(&task_desc_world, "world", 0, 70, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create for task world");
	    return 1;
    }

    rt_task_start(&task_desc_hello, &hello_task, (void*)&sem);
    rt_task_start(&task_desc_world, &world_task, (void*)&sem);

    // Broadcast du sémaphore pour libérer toutes les tâches en attente.
    rt_sem_broadcast(&sem);

    // On attend la fin des tâches et on quite le programme
    rt_task_join(&task_desc_hello);
    rt_task_join(&task_desc_world);

    rt_task_delete(&task_desc_hello);
    rt_task_delete(&task_desc_world);

    return 0;
}
```

Le programme crée un sémaphore à 0 (déjà bloqué).
On choisit pour ce sémaphore le mode `PRIO` pour que les tâches ne soient pas effectuées dans leur ordre de lancement mais dans leur ordre de priorité.

On crée ensuite les tâches avec leurs priorités respectives (ici plus pour `hello` que l'on veut voir avant `world`).
On leur passe en paramètre le sémaphore.

À leur lancement, les tâches bloquent sur le sémaphore.
Enfin, on utilise `rt_sem_broadcast` pour libérer les tâches : cette fonction libère toutes les tâches en attente sur le sémaphore et remet sont compteur à 0.

Les tâches se lancent ensuite dans leur ordre de priorité. Si on changeait les priorités, la tâche `world` pourrait ainsi s'exécuter avant la tâche `hello`.

### Question 2.6

On modifie le programme pour que l'affichage se fasse en boucle, avec une tâche de synchronisation qui servira de métronome.

On ajoute pour cela au programme une tâche `timer_task` qui sera chargée de libérer le sémaphore (via un broadcast) toutes les secondes.
Les tâches `hello` et `world` contiennent maintenant une boucle infinie.

```c
#include <stdio.h>
#include <unistd.h>

#include <sys/mman.h>

#include <native/task.h>
#include <native/sem.h>
#include <rtdk.h>

void hello_task(void* cookie) {
    RT_SEM* sem = (RT_SEM*) cookie;
    while(1) {
        rt_sem_p(sem, TM_INFINITE);
        rt_printf("Hello ");
    }
}

void world_task(void* cookie) {
    RT_SEM* sem = (RT_SEM*) cookie;
    while(1) {
        rt_sem_p(sem, TM_INFINITE);
        rt_printf("world\n");
    }
}

// La tâche timer broadcast le sémaphore toutes les secondes
void timer_task(void* cookie) {
    RT_SEM* sem = (RT_SEM*) cookie;
    while(1) {
        rt_sem_broadcast(sem);
        rt_task_sleep(1000000000);
    }
}

int main() {
    // Flag, descripteurs de tâche et sémaphore
    int err;
    RT_TASK task_desc_hello;
    RT_TASK task_desc_world;
    RT_TASK task_desc_timer;
    RT_SEM sem;

    err = rt_sem_create(&sem, "sem", 0, S_PRIO);
    if(err != 0) {
        fprintf(stderr, "Error in rt_sem_create");
        return 1;
    }

    mlockall(MCL_CURRENT | MCL_FUTURE);
    rt_print_auto_init(1);

    err = rt_task_create(&task_desc_hello, "hello", 0, 80, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create for task hello");
	    return 1;
    }

    err = rt_task_create(&task_desc_world, "world", 0, 70, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create for task world");
	    return 1;
    }

    err = rt_task_create(&task_desc_timer, "timer", 0, 10, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create for task timer");
	    return 1;
    }

    rt_task_start(&task_desc_hello, &hello_task, (void*)&sem);
    rt_task_start(&task_desc_world, &world_task, (void*)&sem);
    rt_task_start(&task_desc_timer, &timer_task, (void*)&sem);

    rt_task_join(&task_desc_hello);
    rt_task_join(&task_desc_world);
    rt_task_join(&task_desc_timer);

    rt_task_delete(&task_desc_hello);
    rt_task_delete(&task_desc_world);
    rt_task_delete(&task_desc_timer);

    return 0;
}
```

Le programme affiche en boucle `Hello world`, toutes les 1s.
Pour les tâches `hello` et `world`, le fonctionnement du programme est similaire à celui du programme précédent, excepté qu'elles consistent désormais en une boucle infinie.

À chaque itération, elles attendent la libération du sémaphore pour pouvoir écrire.

On a ajouté au programme une tâche `timer`, qui en boucle broadcast le sémaphore puis attend unse seconde.
Ainsi, toutes les secondes le sémaphore est libéré et les tâches `hello` et `world` peuvent se relancer.

Pour les priorités des tâches, on donnera une priorité plus élevée à `hello` qu'à `world` pour que les messages soient affichée dans le bon ordre.
`timer` peut disposer d'un faible priorité : `hello` et `world` devront de toute façon l'attendre à cause du sémaphore.

### Question 2.7

```
root@devkit8600-xenomai:~# cat /proc/xenomai/sched
CPU  PID    CLASS  PRI      TIMEOUT   TIMEBASE   STAT       NAME
  0  0      idle    -1      -         master     R          ROOT
  0  1066   rt      80      -         master     W          hello
  0  1067   rt      70      -         master     W          world
  0  1068   rt      10      439ms861us  master     D          timer
root@devkit8600-xenomai:~# cat /proc/xenomai/stat
CPU  PID    MSW        CSW        PF    STAT       %CPU  NAME
  0  0      0          493        0     00500080  100.0  ROOT
  0  1066   0          22         0     00300182    0.0  hello
  0  1067   0          22         0     00300182    0.0  world
  0  1068   0          42         0     00300184    0.0  timer
  0  0      0          43044      0     00000000    0.0  IRQ68: [timer]
```

On note dans les deux fichier la présence des tâches `hello`, `world` et `timer` qui sont des tâches temps réel.

Dnas `sched`, on peut voir que les tâches `hello` et `world` ont pour état `W` (waiting) : elles attendent la libération su sémaphore.
La tâche `timer` quand à elle est dans l'état `D` (delayed) avec un timeout de ~439ms : c'est le `rt_task_sleep`.

La tache `timer` a été context switch moins de fois que les deux autres. Chacune des tâches `hello` et `world` est en effet lancée une fois par `main`, puis par `timer`.

`timer` est context switch deux fois pour pouvoir lancer les autres : juste avant de lancer le timer et une fois que celui-ci est expiré.

Le déroulement du programme et des tâches est le suivant :

main -> hello(bloque) -> main -> world(bloque) -> main -> timer(sleep) -> tâches non tr (OS) -> timer(fin timer) -> hello(print -> bloque) -> world (print -> bloque) -> timer(sleep) -> tâches non tr (OS)...

## Exercice 3

L'objectif de cet exercice est de comparer la latence de Xenomai à celle de Linux mesurée dans le TP précédent.

### Question 3.1

```c
#include <stdio.h>
#include <unistd.h>

#include <sys/mman.h>

#include <native/task.h>
#include <native/timer.h>
#include <rtdk.h>

// La tâche test_task mesure attend 10 000 fois 1ms
void test_task() {
    // On utilise des mesures temps réel de Xenomai (en ns)
    RTIME start = rt_timer_read();
    for(size_t i = 0; i < 10000; i++) {
        rt_task_sleep(1000000);
    }
    RTIME end = rt_timer_read();
    rt_printf("Temps total : %llu\n", (end - start));
}

int main() {

    RT_TASK task_desc;

    mlockall(MCL_CURRENT | MCL_FUTURE);
    rt_print_auto_init(1);

    int err = rt_task_create(&task_desc, "test_task", 0, 99, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create");
	return 1;
    }

    rt_task_start(&task_desc, &test_task, NULL);

    rt_task_join(&task_desc);

    rt_task_delete(&task_desc);

    return 0;
}
```

Sans stress, le temps total est de 10,006512720s contre 10,638069 avec Linux.
Avec un `stress --cpu 500`, le temps est de 10,027916480 soit un changement très faible par rapport aux 67s sous Linux.

On explique cela par le fait que notre programme faisant la mesure est une tâche temps réel alors que le stress est une tâche Linux non temps réel.
Notre tâche est donc prioritaire et complètement éxécutée avant le stress.

### Question 3.2

En modifiant le code pour mesurer les min / max / moyenne, le résultat obtenu est :

```
Temps min : 999320 temps moy : 1001865, temps max : 1031920
```

Avec Linux, on avait :

```
Temps min : 1061000 moyen : 1064000 max : 1458000
```

(avec les temps en ns).

On note que le min et le max sont beaucoup moins espacés sous Xenomai, avec une moyenne plus proche de ce qui était demandé.

Avec un `stress --cpu 500` on a :

```
Temps min : 999800 temps moy : 1004154, temps max : 1028760
```

ce qui est quasiment identique au résultat précédent.

Sous Linux on avait :

```
Temps min : 1028000 moyen : 5299000 max : 857851000
```

On peut en conclure que les tâches OS non temps réel n'ont quasiment aucune incidence sur les tâches avec une priorité temps réel.
