---
title: MI11 TP Linux 2
author: Rémy Huet
date: 6 juin 2021

geometry:
  - left=20mm
  - right=20mm
  - top=30mm
  - bottom=30mm
header-includes:
  - \usepackage{fvextra}
  - \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
---


# MI11 TP2 Linux

## Hello world

### Question 1.1

Le fichier compilé est un ELF 64bits pour x86-64. Il ne fonctionnera donc pas sur la cible qui n'a pas la même architecture.

### Question 1.2

Pour mettre en place la chaîne de compilation croisée, il faut sourcer `/opt/poky/2.3.4/armv7athf-neon
/environment-setup-armv7ahf-neon-poky-linux-gnueabi`.


### Question 1.3

Le fichier cimpilé est maintenant un ELF32 bits pour ARM EABI5.
Il peut donc être exécuté sur la cible (mais plus sur le PC hôte).

### Question 1.4

```c
#include <stdio.h>

int main() {
    printf("Hello world\n");

    return 0;
}
```

Cela affiche bien « Hello world ».

## Clignottement des LEDs

### Question 2.1

On peut accédes aux leds par les fichiers `/sys/class/leds/user_led/brightness `(ou `sys_led`).
Il suffit d'écrire 0 ou 1 dans ces fichiers pour changer l'état de la LED.

### Question 2.2

```console
# echo 1 > /sys/class/leds/user_led/brightness
# echo 1 > /sys/class/leds/sys_led/brightness
# echo 0 > /sys/class/leds/user_led/brightness
# echo 0 > /sys/class/leds/sys_led/brightness
```

### Question 2.3

Le code C pour faire clignotter les LEDs est donné ci-après :

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/** @brief Écrire i dans le fichier de la led user
 *
 * @details Écrire 1 pour allumer la LED, 0 pour l'éteindre
 * @param i le nombre à écrire
 */
void write_user(int i) { 
    // Ouverture du fichier
    FILE* fptr;
    fptr = fopen("/sys/class/leds/user_led/brightness", "w");
    // En cas d'erreur :
    if(fptr == NULL) {
        fprintf(stderr, "Impossible d'écrire dans la led user");
   	exit(1);
    }

    // Écrire i dans le fichier (sous forme de chaîne de caractères)
    fprintf(fptr, "%d", i);

    // Fermeture du fichier
    fclose(fptr);
}

/** @brief Écrire i dans le fichier de la led système
 *
 * @details Écrire 1 pour allumer la LED, 0 pour l'éteindre
 * @param i le nombre à écrire
 */
void write_sys(int i) { 
    FILE* fptr;
    fptr = fopen("/sys/class/leds/sys_led/brightness", "w");
    if(fptr == NULL) {
        fprintf(stderr, "Impossible d'écrire dans la led sys");
   	exit(1);
    }

    fprintf(fptr, "%d", i);

    fclose(fptr);
}

int main() {
    // À l'infini
    while(1) {
        // Allumer la LED user et éteindre la LED sys
        write_user(1);
	    write_sys(0);
        // Attendre 1 seconde
	    sleep(1);
        // Allumer la LED sys et étindre la LED user
	    write_user(0);
	    write_sys(1);
        // Attendre 1 seconde
	    sleep(1);
    }

    // Fin du programme (jamais atteinte)
    return 0;
}
```

## Exercice 3 : boutons poussoirs

### Quesition 3.1

Il y a 4 boutons poussoir sur la devkit : 3 utilisables par l'utilisateur et le bouton reset.
On y accède via `/dev/input/event1`.

### Question 3.2

On peut tester les événements via la commande `evtest`.
Celle ci affiche ensuite à chaque événement :

- Son timestamp
- Son type (ici toujours `EV_KEY`)
- Son code (qui correspond au bouton sur lequel on appuie)
- Sa valeur (1 pour poussé, 0 pour relaché).

Ex :

```
Event: time 1621261232.795473, type 1 (EV_KEY), code 102 (KEY_HOME), value 0
Event: time 1621261232.795475, -------------- SYN_REPORT ------------
Event: time 1621261237.319190, type 1 (EV_KEY), code 102 (KEY_HOME), value 1
Event: time 1621261237.319193, -------------- SYN_REPORT ------------
Event: time 1621261237.668050, type 1 (EV_KEY), code 102 (KEY_HOME), value 0
Event: time 1621261237.668052, -------------- SYN_REPORT ------------
Event: time 1621261239.871364, type 1 (EV_KEY), code 59 (KEY_F1), value 1
Event: time 1621261239.871367, -------------- SYN_REPORT ------------
Event: time 1621261240.076995, type 1 (EV_KEY), code 59 (KEY_F1), value 0
Event: time 1621261240.076998, -------------- SYN_REPORT ------------
Event: time 1621261240.078025, type 1 (EV_KEY), code 59 (KEY_F1), value 1
Event: time 1621261240.078028, -------------- SYN_REPORT ------------
Event: time 1621261240.078658, type 1 (EV_KEY), code 59 (KEY_F1), value 0
Event: time 1621261240.078660, -------------- SYN_REPORT ------------
Event: time 1621261241.741791, type 1 (EV_KEY), code 1 (KEY_ESC), value 1
Event: time 1621261241.741794, -------------- SYN_REPORT ------------
Event: time 1621261241.929407, type 1 (EV_KEY), code 1 (KEY_ESC), value 0
Event: time 1621261241.929411, -------------- SYN_REPORT ------------
```

### Question 3.3

Le `struct input_event` est déclaré dans le header 

`/opt/poky/2.3.4/armv7athf-neon/sysroots/armv7ahf-neon-poky-linux-gnueabi/
usr/include/linux/input.h`.

Pour l'importer on fera donc un `#include <linux/input.h>`.

### Question 3.4

Le code C pour les tests des boutons est donné ci-après :

```c
#include <stdio.h>
#include <stdlib.h>
#include <linux/input.h>

int main() {
    // On va lire les événements dans la structure suivante
    struct input_event event;
    
    // Ouverture du fichier pour la lecture des événements
    FILE* fptr = fopen("/dev/input/event1", "rb");
    if (fptr == NULL) {
        fprintf(stderr, "Impossible d'ouvrir /dev/input/event1");
        exit(1);
    }

    // Boucle infinie
    while(1) {
        // On lit un événement depuis le fichier (lecture blocante)
        fread(&event, sizeof(struct input_event), 1, fptr);
        // On affiche quelque chose différent en fonction de son code et sa valeur
        switch(event.code) {
            case 0:
                printf("Sync event !\n");
                break;
            case 1:
                if (event.value == 0)
                    printf("KEY_ESC relachée \n");
                else
                    printf("KEY_ESC pressé \n");
                break;
            case 59:
                if (event.value == 0)
                    printf("KEY_F1 relachée \n");
                else
                    printf("KEY_F1 pressé \n");
                break;
            case 102:
                if (event.value == 0)
                    printf("KEY_HOME relachée \n");
                else
                    printf("KEY_HOME pressé \n");
                break;
        }
    }

    // Fin du programme (jamais atteinte)
    fclose(fptr);
    return 0;
}
```

## Exercice 4 : charge CPU

### Question 4.1

Temps sans stress : 10 s 638069 us (varie un peu)

On ajoute un stress avec --cpu=x.
En fonction de x, le temps du programme devient:

- 10 : 10 s 624701 us
- 100 : 12 s 506414 us
- 200 : 24 s 698193 us
- 500 : 67 s 963570 us

### Question 4.2

Sans stress : Temps min : 1061 moyen : 1064 max : 1458

- 10 : Temps min : 1017 moyen : 1063 max : 2775
- 100 : Temps min : 1038 moyen : 1519 max : 225389
- 200 : Temps min : 1027 moyen : 3012 max : 935013
- 500 : Temps min : 1028 moyen : 5299 max : 857851

On pourrait améliorer les résultats en donnant une priorité plus élevée à notre programme qu'au stress.

### Question 4.3

Le code source du programme (question 4.2) est donné ci-après :

```c
#include <stdio.h>
#include <limits.h>
#include <unistd.h>
#include <sys/time.h>

// Nombre d'itérations
const int ITERATIONS = 10000;

int main() {

    // Timestamp de début et fin d'itération, différence
    struct timeval start_time, end_time, diff;
    int micros_total = 0;
    int micros_max = 0;
    int micros_min = INT_MAX;

    // On répère nombre ITERATIONS fois
    for(int i = 0; i < ITERATIONS; i++) {
        // Date avant le usleep
        gettimeofday(&start_time, NULL);
        // On dort 1ms
        usleep(1000);
        // Date après le usleep
        gettimeofday(&end_time, NULL);
        // Calcul de la différence
        timersub(&end_time, &start_time, &diff);
        
        // On considère qu'il n'y aura pas de secondes pour simplifier
        int micros = diff.tv_usec;
        
        // Total
        micros_total += micros;

        // Min / max
        if (micros_min > micros)
            micros_min = micros;
        if (micros_max < micros)
            micros_max = micros;
    }

    // Affichage temps min / max / moyenne
    printf("Temps min : %d moyen : %d max : %d\n", micros_min, micros_total / ITERATIONS, micros_max);

    return 0;
}
```

## Exercice 5

### Question 5.1

Le fichier à manipuler pour le touchscreen est `/dev/input/event0`.

### Question 5.2

Le touchscreen nous donne des événements pour l'appui (position X, Y et force) et un événement lorsque l'on arrête d'appuyer.

### Question 5.3

On manipule le rétroéclairage avec `/sys/class/backlight/pwm-backlight/brightness`.

### Question 5.4

Exemple de manipulation du rétroéclairage :

```console
# echo 0 > /sys/class/backlight/pwm-backlight/brightness
# echo 50 > /sys/class/backlight/pwm-backlight/brightness
# echo 100 > /sys/class/backlight/pwm-backlight/brightness
```

### Question 5.5

```c
#include <stdio.h>
#include <stdlib.h>
#include <linux/input.h>

// Calibrage logiciel pour le touchscreen
const int MIN_X = 200;
const int MAX_X = 3800;

int main() {
    struct input_event event;
    
    // Ouverture du fichier pour les événements
    FILE* ptrscreen = fopen("/dev/input/event0", "rb");
    if (ptrscreen == NULL) {
        fprintf(stderr, "Impossible d'ouvrir /dev/input/event0");
        exit(1);
    }

    // Boucle infinie
    while(1) {
        // Lecture bloquante d'un événement
        fread(&event, sizeof(struct input_event), 1, ptrscreen);
        
        // On ne s'intéresse qu'aux événement indiquant la position d'une pression sur l'axe X
        if(event.type == 3 && event.code == 0) {
            float x = event.value - MIN_X;
            // Application des bornes et calcul de la luminisité
            if (x < 0)
                x = 0;
            if (x > MAX_X)
                x = MAX_X;

            int brightness = 100 - (int)(x * 100 / (MAX_X - MIN_X));

            if (brightness < 0)
                brightness = 0;
            if (brightness > 100)
                brightness = 100;

            // Ouverture du fichier et écriture de la luminosité
            FILE* ptrbrightness = fopen("/sys/class/backlight/pwm-backlight/brightness", "w");
            if (ptrbrightness == NULL) {
                fprintf(stderr, "Impossible d'ouvrir /sys/class/backlight/pwm-backlight/brightness");
                exit(1);
            }
            fprintf(ptrbrightness, "%d", brightness);
            fclose(ptrbrightness);
        }
    }

    fclose(ptrscreen);
    return 0;
}
```


oalanism@etu.utc.fr
