#include <stdio.h>
#include <unistd.h>
#include <limits.h>

#include <sys/mman.h>

#include <native/task.h>
#include <native/timer.h>
#include <rtdk.h>

void test_task() {
    RTIME min = LLONG_MAX;
    RTIME max = 0;
    RTIME sum = 0;
    for(size_t i = 0; i < 10000; i++) {
        RTIME start = rt_timer_read();
        rt_task_sleep(1000000);
        RTIME end = rt_timer_read();
        RTIME time = end - start;
        sum += time;
        if (min > time)
            min = time;
        if (max < time)
            max = time;
    }
    rt_printf("Temps min : %llu temps moy : %llu, temps max : %llu\n", min, sum / 10000, max);
}

int main() {
    
    RT_TASK task_desc;

    mlockall(MCL_CURRENT | MCL_FUTURE);
    rt_print_auto_init(1);

    int err = rt_task_create(&task_desc, "test_task", 0, 99, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create");
	return 1;
    }

    rt_task_start(&task_desc, &test_task, NULL);

    rt_task_join(&task_desc);

    rt_task_delete(&task_desc);

    return 0;
}
