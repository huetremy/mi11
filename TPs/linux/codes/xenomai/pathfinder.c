#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>

#include <native/mutex.h>
#include <native/sem.h>
#include <native/task.h>
#include <native/timer.h>

#include <rtdk.h>

#define TASK_MODE T_JOINABLE
#define TASK_STKSZ 0

#define BIEN_CODE

#ifdef BIEN_CODE
static RT_MUTEX mutex;
#else // Oups la NASA
static RT_SEM sem;
#endif


static RT_SEM distrib_exec;

typedef struct task_descriptor {
  RT_TASK task;
  void (*task_function)(void *);
  RTIME period;
  RTIME duration;
  int priority;
  bool use_resource;
} task_descriptor;

///////////////////////////////////////////////////////////
char *rt_task_name(void) {
  static RT_TASK_INFO info;
  rt_task_inquire(NULL, &info);

  return info.name;
}

int rt_task_prio(void) {
  static RT_TASK_INFO info;
  rt_task_inquire(NULL, &info);

  return info.cprio;
}

///////////////////////////////////////////////////////////
int ms_time_since_start(void) {
  static RTIME init_time = 0;

  if (init_time == 0)
    init_time = rt_timer_read();

  return (rt_timer_read() - init_time) / 1000000;
}

int ns_time_since_start(void) {
  static RTIME init_time = 0;

  if (init_time == 0)
    init_time = rt_timer_read();

  return (rt_timer_read() - init_time);
}

///////////////////////////////////////////////////////////
void acquire_resource(void) {
  #ifdef BIEN_CODE
  rt_mutex_acquire(&mutex, TM_INFINITE);
  #else
  rt_sem_p(&sem, TM_INFINITE);
  #endif
}

///////////////////////////////////////////////////////////
void release_resource(void) {
  #ifdef BIEN_CODE
  rt_mutex_release(&mutex);
  #else
  rt_sem_v(&sem);
  #endif
}

///////////////////////////////////////////////////////////
void busy_wait(RTIME time) {
  RT_TASK_INFO info;
  rt_task_inquire(NULL, &info);
  RTIME start = info.exectime;
  while(1) {
    rt_task_inquire(NULL, &info);
    if(info.exectime > start + time)
      break;
  }
}

///////////////////////////////////////////////////////////
void rt_task(void *cookie) {
  struct task_descriptor *params = (struct task_descriptor *)cookie;

  rt_printf("started task %s, period %ims, duration %ims, use resource %i\n",
            rt_task_name(), (int)(params->period / 1000000),
            (int)(params->duration / 1000000), params->use_resource);

  while (1) {
    rt_task_wait_period(NULL);
    if (params->use_resource)
      acquire_resource();
    rt_printf("doing %s at %d ms with prio %d \n", rt_task_name(), ms_time_since_start(), rt_task_prio());
    busy_wait(params->duration);
    rt_printf("doing %s ok at %d ms with prio %d\n", rt_task_name(), ms_time_since_start(), rt_task_prio());
    if (params->use_resource)
      release_resource();
  }
}

///////////////////////////////////////////////////////////
int create_and_start_rt_task(struct task_descriptor *desc, char *name) {
  int status =
      rt_task_create(&desc->task, name, TASK_STKSZ, desc->priority, TASK_MODE);
  if (status != 0) {
    fprintf(stderr, "error creating task %s\n", name);
    return status;
  }

  status = rt_task_set_periodic(&desc->task, TM_NOW, desc->period);
  if (status != 0) {
    fprintf(stderr, "error setting period on task %s\n", name);
    return status;
  }

  status = rt_task_start(&desc->task, desc->task_function, desc);
  if (status != 0) {
    fprintf(stderr, "error starting task %s\n", name);
  }
  return status;
}

///////////////////////////////////////////////////////////
void ordo_task(void* cookie) {
  struct task_descriptor *params = (struct task_descriptor *)cookie;
  RT_SEM_INFO sem_info;

  rt_printf("started task %s, period %ims, duration %ims, use resource %i\n",
            rt_task_name(), (int)(params->period / 1000000),
            (int)(params->duration / 1000000), params->use_resource);

  while (1) {
    rt_task_wait_period(NULL);
    rt_printf("doing %s at %d ms\n", rt_task_name(), ms_time_since_start());
    rt_sem_inquire(&distrib_exec, &sem_info);
    if(sem_info.count == 0) {
      fprintf(stderr, "Sécurité ! Distrib ne s'est pas exécutée entièrement entre deux ordonancements\n");
      exit(2);
    }
    busy_wait(params->duration);
    rt_printf("doing %s ok at %d ms\n", rt_task_name(), ms_time_since_start());
  }
}

void distrib_task(void* cookie) {
  struct task_descriptor *params = (struct task_descriptor *)cookie;

  rt_printf("started task %s, period %ims, duration %ims, use resource %i\n",
            rt_task_name(), (int)(params->period / 1000000),
            (int)(params->duration / 1000000), params->use_resource);

  while (1) {
    rt_task_wait_period(NULL);
    if (params->use_resource)
      acquire_resource();
    rt_sem_p(&distrib_exec, TM_INFINITE);
    rt_printf("doing %s at %d ms\n", rt_task_name(), ms_time_since_start());
    busy_wait(params->duration);
    rt_printf("doing %s ok at %d ms\n", rt_task_name(), ms_time_since_start());
    if (params->use_resource)
      release_resource();
    rt_sem_v(&distrib_exec);
  }
}

///////////////////////////////////////////////////////////
int main(void) {
  int err;
  /* Avoids memory swapping for this program */
  mlockall(MCL_CURRENT | MCL_FUTURE);

  rt_print_auto_init(1);

  #ifdef BIEN_CODE
  // init mutex
  err = rt_mutex_create(&mutex, "mutex");
  if(err != 0) {
    fprintf(stderr, "Erreur à la création du mutex ressource\n");
    return EXIT_FAILURE;
  }
  #else
  // init sem
  err = rt_sem_create(&sem, "sem", 1, S_PRIO);
  if(err != 0) {
    fprintf(stderr, "Erreur à la création du sémaphore ressource\n");
    return EXIT_FAILURE;
  }
  #endif

  err = rt_sem_create(&distrib_exec, "distrib_exec", 1, S_PRIO);
  if(err != 0) {
    fprintf(stderr, "Erreur à la création du sémaphore sécurité\n");
    return EXIT_FAILURE;
  }

  // Create and launch ORDO_BUS task
  RT_TASK ordo_rt;
  task_descriptor ordo_td = {
    .task = ordo_rt,
    .task_function = ordo_task,
    .period = 125000000,
    .duration = 25000000,
    .priority = 7,
    .use_resource = 0,
  };
  create_and_start_rt_task(&ordo_td, "ORDO_BUS");

  RT_TASK distrib_rt;
  task_descriptor distrib_td = {
    .task = distrib_rt,
    .task_function = distrib_task,
    .period = 125000000,
    .duration = 25000000,
    .priority = 6,
    .use_resource = 1,
  };
  create_and_start_rt_task(&distrib_td, "DISTRIB_DONNEES");

  RT_TASK pilotage_rt;
  task_descriptor pilotage_td = {
    .task = pilotage_rt,
    .task_function = rt_task,
    .period = 250000000,
    .duration = 25000000,
    .priority = 5,
    .use_resource = 1,
  };
  create_and_start_rt_task(&pilotage_td, "PILOTAGE");

  RT_TASK radio_rt;
  task_descriptor radio_td = {
    .task = radio_rt,
    .task_function = rt_task,
    .period = 250000000,
    .duration = 25000000,
    .priority = 4,
    .use_resource = 0,
  };
  create_and_start_rt_task(&radio_td, "RADIO");

  RT_TASK camera_rt;
  task_descriptor camera_td = {
    .task = camera_rt,
    .task_function = rt_task,
    .period = 250000000,
    .duration = 25000000,
    .priority = 3,
    .use_resource = 0,
  };
  create_and_start_rt_task(&camera_td, "CAMERA");

  RT_TASK mesures_rt;
  task_descriptor mesures_td = {
    .task = mesures_rt,
    .task_function = rt_task,
    .period = 5000000000,
    .duration = 50000000,
    .priority = 2,
    .use_resource = 1,
  };
  create_and_start_rt_task(&mesures_td, "MESURES");

  RT_TASK meteo_rt;
  task_descriptor meteo_td = {
    .task = meteo_rt,
    .task_function = rt_task,
    .period = 5000000000,
    .duration = 60000000,
    .priority = 1,
    .use_resource = 1,
  };
  create_and_start_rt_task(&meteo_td, "METEO");

  while(1) continue;

  return EXIT_SUCCESS;
}
