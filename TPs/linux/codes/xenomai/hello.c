#include <stdio.h>
#include <unistd.h>

#include <sys/mman.h>

#include <native/task.h>
#include <rtdk.h>

void hello_task() {
    while(1) {
        printf("Hello world !\n");
	sleep(1);
    }
}

int main() {
    
    RT_TASK task_desc;

    mlockall(MCL_CURRENT | MCL_FUTURE);
    // rt_print_auto_init(1);

    int err = rt_task_create(&task_desc, "hello", 0, 99, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create");
	return 1;
    }

    rt_task_start(&task_desc, &hello_task, NULL);

    rt_task_join(&task_desc);

    rt_task_delete(&task_desc);

    return 0;
}
