#include <stdio.h>
#include <unistd.h>

#include <sys/mman.h>

#include <native/task.h>
#include <native/sem.h>
#include <rtdk.h>

void hello_task(void* cookie) {
    RT_SEM* sem = (RT_SEM*) cookie;
    while(1) {
        rt_sem_p(sem, TM_INFINITE);
        rt_printf("Hello ");
    }
}

void world_task(void* cookie) {
    RT_SEM* sem = (RT_SEM*) cookie;
    while(1) {
        rt_sem_p(sem, TM_INFINITE);
        rt_printf("world\n");
    }
}

void timer_task(void* cookie) {
    RT_SEM* sem = (RT_SEM*) cookie;
    while(1) {
        rt_sem_broadcast(sem);
        rt_task_sleep(1000000000);
    }
}

int main() {
    int err;
    RT_TASK task_desc_hello;
    RT_TASK task_desc_world;
    RT_TASK task_desc_timer;
    RT_SEM sem;

    err = rt_sem_create(&sem, "sem", 0, S_PRIO);

    mlockall(MCL_CURRENT | MCL_FUTURE);
    rt_print_auto_init(1);

    err = rt_task_create(&task_desc_hello, "hello", 0, 80, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create for task hello");
	    return 1;
    }

    err = rt_task_create(&task_desc_world, "world", 0, 81, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create for task world");
	    return 1;
    }

    err = rt_task_create(&task_desc_timer, "timer", 0, 10, T_JOINABLE);
    if(err != 0) {
        fprintf(stderr, "Error in rt_task_create for task timer");
	    return 1;
    }

    rt_task_start(&task_desc_hello, &hello_task, (void*)&sem);
    rt_task_start(&task_desc_world, &world_task, (void*)&sem);
    rt_task_start(&task_desc_timer, &timer_task, (void*)&sem);

    rt_task_join(&task_desc_hello);
    rt_task_join(&task_desc_world);
    rt_task_join(&task_desc_timer);

    rt_task_delete(&task_desc_hello);
    rt_task_delete(&task_desc_world);
    rt_task_delete(&task_desc_timer);

    return 0;
}
