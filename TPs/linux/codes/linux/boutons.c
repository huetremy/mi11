#include <stdio.h>
#include <stdlib.h>
#include <linux/input.h>

int main() {
    struct input_event event;
    
    FILE* fptr = fopen("/dev/input/event1", "rb");
    if (fptr == NULL) {
        fprintf(stderr, "Impossible d'ouvrir /dev/input/event1");
        exit(1);
    }

    while(1) {
        fread(&event, sizeof(struct input_event), 1, fptr);
        switch(event.code) {
            case 0:
                printf("Sync event !\n");
                break;
            case 1:
                if (event.value == 0)
                    printf("KEY_ESC relachée \n");
                else
                    printf("KEY_ESC pressé \n");
                break;
            case 59:
                if (event.value == 0)
                    printf("KEY_F1 relachée \n");
                else
                    printf("KEY_F1 pressé \n");
                break;
            case 102:
                if (event.value == 0)
                    printf("KEY_HOME relachée \n");
                else
                    printf("KEY_HOME pressé \n");
                break;
        }
    }

    fclose(fptr);
    return 0;
}
