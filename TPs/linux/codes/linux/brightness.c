#include <stdio.h>
#include <stdlib.h>
#include <linux/input.h>

const int MIN_X = 200;
const int MAX_X = 3800;

int main() {
    struct input_event event;
    
    FILE* ptrscreen = fopen("/dev/input/event0", "rb");
    if (ptrscreen == NULL) {
        fprintf(stderr, "Impossible d'ouvrir /dev/input/event0");
        exit(1);
    }

    while(1) {
        fread(&event, sizeof(struct input_event), 1, ptrscreen);
        if(event.type == 3 && event.code == 0) {
            float x = event.value - MIN_X;
            if (x < 0)
                x = 0;
            if (x > MAX_X)
                x = MAX_X;

            int brightness = 100 - (int)(x * 100 / (MAX_X - MIN_X));

            if (brightness < 0)
                brightness = 0;
            if (brightness > 100)
                brightness = 100;

            FILE* ptrbrightness = fopen("/sys/class/backlight/pwm-backlight/brightness", "w");
            if (ptrbrightness == NULL) {
                fprintf(stderr, "Impossible d'ouvrir /sys/class/backlight/pwm-backlight/brightness");
                exit(1);
            }
            fprintf(ptrbrightness, "%d", brightness);
            fclose(ptrbrightness);
        }
    }

    fclose(ptrscreen);
    return 0;
}
