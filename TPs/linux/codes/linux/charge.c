#include <stdio.h>
#include <limits.h>
#include <unistd.h>
#include <sys/time.h>

const int ITERATIONS = 10000;


int main() {

    struct timeval start_time, end_time, diff;
    int micros_total = 0;
    int micros_max = 0;
    int micros_min = INT_MAX;

    for(int i = 0; i < ITERATIONS; i++) {
        gettimeofday(&start_time, NULL);
        usleep(1000);
        gettimeofday(&end_time, NULL);
        timersub(&end_time, &start_time, &diff);
        
        int micros = diff.tv_usec;
        micros_total += micros;

        if (micros_min > micros)
            micros_min = micros;
        if (micros_max < micros)
            micros_max = micros;
    }

    printf("Temps min : %d moyen : %d max : %d\n", micros_min, micros_total / ITERATIONS, micros_max);

    return 0;
}
