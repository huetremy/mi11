---
title: MI11 TP Xenomai 2 (pathfinder)
author: Rémy \textsc{Huet} (travail rendu seul)
date: 21 juin 2021

geometry:
  - left=20mm
  - right=20mm
  - top=30mm
  - bottom=30mm
header-includes:
  - \usepackage{fvextra}
  - \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
---

# MI11 TP Xenomai 2

## Question 1

La structure `task_descriptor` est utilisée par le programme pour stocker les informations des tâches.
Elle contient:

- le descripteur de tâche de Xenomai `RT_TASK`;
- la fonction correspondant à la tâche
- la période et la durée estimée de la tâche
- la priorité de la tâche
- un flag indiquant si la tâche utilise la ressource.

La fonction `rt_task` est une fonction générique pour les tâches que nous allons simuler : elle utlise le `struct task_descriptor` pour savoir quoi faire (temps d'attente, durée, affichage de messages de debug...).
Toutes les tâches utiliseront ce fonctionnement général avec leur propre configuration.

La fonction `create_and_start_rt_task` prend en paramètre un nom et un descripteur de tâche, crée la tâche Xenomai, la règle comme une tâche périodique et la lance.

## Question 2

Laa fonction `rt_task_name` fait appel à la fonction `rt_task_inquire` de Xenomai pour récupérer une structure RT_TASK_INFO contenant des informations sur la tâche courante (`NULL` en premier paramètre).

Cette structure contient des informations sur la tâche, dont som nom.

## Question 3

La fonciton busy_wait additionne le temps auquel elle commence au temps qu'elle doit attendre pour savoir quand elle doit terminer.
Elle fait ensuite une boucle d'attente active tant que `ns_time_since_start()` est inférieur à l'heure de fin.

## Question 4

En ajoutant les timings dans les logs, on obtient :

```
root@devkit8600-xenomai:~# ./pathfinder
started task ORDO_BUS, period 125ms, duration 25ms, use resource 0
doing ORDO_BUS at 0 ms
doing ORDO_BUS ok at 25 ms
doing ORDO_BUS at 124 ms
doing ORDO_BUS ok at 149 ms
doing ORDO_BUS at 249 ms
doing ORDO_BUS ok at 274 ms
doing ORDO_BUS at 374 ms
doing ORDO_BUS ok at 399 ms
doing ORDO_BUS at 499 ms
doing ORDO_BUS ok at 524 ms
doing ORDO_BUS at 624 ms
doing ORDO_BUS ok at 649 ms
doing ORDO_BUS at 749 ms
doing ORDO_BUS ok at 774 ms
doing ORDO_BUS at 874 ms
doing ORDO_BUS ok at 899 ms
doing ORDO_BUS at 999 ms
doing ORDO_BUS ok at 1024 ms
doing ORDO_BUS at 1124 ms
doing ORDO_BUS ok at 1149 ms
doing ORDO_BUS at 1249 ms
doing ORDO_BUS ok at 1274 ms
```

Les timing sont corrects.

## Question 5

Le sémaphore est initialisé à 1 pour servir de Mutex : la ressource est libre au début, chaque tâche la prend et la libère.
L'enchaînement des tâches semble cohérent.

## Question 6

Utilisation d'un sémaphore par distrib données. Si le count est à 0 quand on lance ordo, c'est que distrib données n'est pas finie, la sécurité lève donc une erreur.

## Question 7

- Pour un temps extrème METEO (60ms), la sécurité s'active
- Pour un faible temps de METEO (40ms), pas de soucis.

Les logs sont données ci-après.

```
doing ORDO_BUS at 4999 ms
doing ORDO_BUS ok at 5024 ms
doing DISTRIB_DONNEES at 5024 ms
doing DISTRIB_DONNEES ok at 5050 ms
doing CAMERA ok at 5050 ms
doing MESURES at 5050 ms
doing MESURES ok at 5100 ms
doing METEO at 5100 ms
doing ORDO_BUS at 5124 ms
doing ORDO_BUS ok at 5149 ms
doing RADIO at 5150 ms
doing RADIO ok at 5175 ms
doing CAMERA at 5175 ms
doing CAMERA ok at 5200 ms
doing METEO ok at 5235 ms
doing DISTRIB_DONNEES at 5235 ms
doing ORDO_BUS at 5249 ms
doing DISTRIB_DONNEES ok at 5260 ms
doing PILOTAGE at 5260 ms
doing PILOTAGE ok at 5285 ms
doing DISTRIB_DONNEES at 5285 ms
doing DISTRIB_DONNEES ok at 5310 ms
doing DISTRIB_DONNEES at 5375 ms
doing DISTRIB_DONNEES ok at 5400 ms
doing PILOTAGE at 5400 ms
doing PILOTAGE ok at 5425 ms
doing RADIO at 5425 ms
```

## Question 8

Solution : faire de l'héritage de priorité sur le sémaphore (utiliser un Mutex Xenomai).

## Question 9

Ça marche : quand la tâche DISTRIB_DONNEES demande le mutex, METEO récupère sa priorité.
RADIO et CAMERA ne sont pas exécutées avant la fin de METEO donc plus de conflit.

Ajout de la priorité actuelle des tâches dans les logs

```
doing CAMERA at 4975 ms with prio 3
doing ORDO_BUS at 4999 ms
doing ORDO_BUS ok at 5024 ms
doing DISTRIB_DONNEES at 5024 ms
doing DISTRIB_DONNEES ok at 5049 ms
doing CAMERA ok at 5050 ms with prio 3
doing MESURES at 5050 ms with prio 2
doing MESURES ok at 5100 ms with prio 2
doing METEO at 5100 ms with prio 1
doing ORDO_BUS at 5124 ms
doing ORDO_BUS ok at 5149 ms
doing METEO ok at 5185 ms with prio 6
doing DISTRIB_DONNEES at 5185 ms
doing DISTRIB_DONNEES ok at 5210 ms
doing PILOTAGE at 5210 ms with prio 5
doing PILOTAGE ok at 5235 ms with prio 5
doing RADIO at 5235 ms with prio 4
doing ORDO_BUS at 5249 ms
doing ORDO_BUS ok at 5274 ms
```

## Code final pour le TP

```c
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>

#include <native/mutex.h>
#include <native/sem.h>
#include <native/task.h>
#include <native/timer.h>

#include <rtdk.h>

#define TASK_MODE T_JOINABLE
#define TASK_STKSZ 0

#define BIEN_CODE

#ifdef BIEN_CODE
static RT_MUTEX mutex;
#else // Oups la NASA
static RT_SEM sem;
#endif


static RT_SEM distrib_exec;

typedef struct task_descriptor {
  RT_TASK task;
  void (*task_function)(void *);
  RTIME period;
  RTIME duration;
  int priority;
  bool use_resource;
} task_descriptor;

///////////////////////////////////////////////////////////
char *rt_task_name(void) {
  static RT_TASK_INFO info;
  rt_task_inquire(NULL, &info);

  return info.name;
}

int rt_task_prio(void) {
  static RT_TASK_INFO info;
  rt_task_inquire(NULL, &info);

  return info.cprio;
}

///////////////////////////////////////////////////////////
int ms_time_since_start(void) {
  static RTIME init_time = 0;

  if (init_time == 0)
    init_time = rt_timer_read();

  return (rt_timer_read() - init_time) / 1000000;
}

int ns_time_since_start(void) {
  static RTIME init_time = 0;

  if (init_time == 0)
    init_time = rt_timer_read();

  return (rt_timer_read() - init_time);
}

///////////////////////////////////////////////////////////
void acquire_resource(void) {
  #ifdef BIEN_CODE
  rt_mutex_acquire(&mutex, TM_INFINITE);
  #else
  rt_sem_p(&sem, TM_INFINITE);
  #endif
}

///////////////////////////////////////////////////////////
void release_resource(void) {
  #ifdef BIEN_CODE
  rt_mutex_release(&mutex);
  #else
  rt_sem_v(&sem);
  #endif
}

///////////////////////////////////////////////////////////
void busy_wait(RTIME time) {
  RT_TASK_INFO info;
  rt_task_inquire(NULL, &info);
  RTIME start = info.exectime;
  while(1) {
    rt_task_inquire(NULL, &info);
    if(info.exectime > start + time)
      break;
  }
}

///////////////////////////////////////////////////////////
void rt_task(void *cookie) {
  struct task_descriptor *params = (struct task_descriptor *)cookie;

  rt_printf("started task %s, period %ims, duration %ims, use resource %i\n",
            rt_task_name(), (int)(params->period / 1000000),
            (int)(params->duration / 1000000), params->use_resource);

  while (1) {
    rt_task_wait_period(NULL);
    if (params->use_resource)
      acquire_resource();
    rt_printf("doing %s at %d ms with prio %d \n", rt_task_name(), ms_time_since_start(), rt_task_prio());
    busy_wait(params->duration);
    rt_printf("doing %s ok at %d ms with prio %d\n", rt_task_name(), ms_time_since_start(), rt_task_prio());
    if (params->use_resource)
      release_resource();
  }
}

///////////////////////////////////////////////////////////
int create_and_start_rt_task(struct task_descriptor *desc, char *name) {
  int status =
      rt_task_create(&desc->task, name, TASK_STKSZ, desc->priority, TASK_MODE);
  if (status != 0) {
    fprintf(stderr, "error creating task %s\n", name);
    return status;
  }

  status = rt_task_set_periodic(&desc->task, TM_NOW, desc->period);
  if (status != 0) {
    fprintf(stderr, "error setting period on task %s\n", name);
    return status;
  }

  status = rt_task_start(&desc->task, desc->task_function, desc);
  if (status != 0) {
    fprintf(stderr, "error starting task %s\n", name);
  }
  return status;
}

///////////////////////////////////////////////////////////
void ordo_task(void* cookie) {
  struct task_descriptor *params = (struct task_descriptor *)cookie;
  RT_SEM_INFO sem_info;

  rt_printf("started task %s, period %ims, duration %ims, use resource %i\n",
            rt_task_name(), (int)(params->period / 1000000),
            (int)(params->duration / 1000000), params->use_resource);

  while (1) {
    rt_task_wait_period(NULL);
    rt_printf("doing %s at %d ms\n", rt_task_name(), ms_time_since_start());
    rt_sem_inquire(&distrib_exec, &sem_info);
    if(sem_info.count == 0) {
      fprintf(stderr, "Sécurité ! Distrib ne s'est pas exécutée entièrement entre deux ordonancements\n");
      exit(2);
    }
    busy_wait(params->duration);
    rt_printf("doing %s ok at %d ms\n", rt_task_name(), ms_time_since_start());
  }
}

void distrib_task(void* cookie) {
  struct task_descriptor *params = (struct task_descriptor *)cookie;

  rt_printf("started task %s, period %ims, duration %ims, use resource %i\n",
            rt_task_name(), (int)(params->period / 1000000),
            (int)(params->duration / 1000000), params->use_resource);

  while (1) {
    rt_task_wait_period(NULL);
    if (params->use_resource)
      acquire_resource();
    rt_sem_p(&distrib_exec, TM_INFINITE);
    rt_printf("doing %s at %d ms\n", rt_task_name(), ms_time_since_start());
    busy_wait(params->duration);
    rt_printf("doing %s ok at %d ms\n", rt_task_name(), ms_time_since_start());
    if (params->use_resource)
      release_resource();
    rt_sem_v(&distrib_exec);
  }
}

///////////////////////////////////////////////////////////
int main(void) {
  int err;
  /* Avoids memory swapping for this program */
  mlockall(MCL_CURRENT | MCL_FUTURE);

  rt_print_auto_init(1);

  #ifdef BIEN_CODE
  // init mutex
  err = rt_mutex_create(&mutex, "mutex");
  if(err != 0) {
    fprintf(stderr, "Erreur à la création du mutex ressource\n");
    return EXIT_FAILURE;
  }
  #else
  // init sem
  err = rt_sem_create(&sem, "sem", 1, S_PRIO);
  if(err != 0) {
    fprintf(stderr, "Erreur à la création du sémaphore ressource\n");
    return EXIT_FAILURE;
  }
  #endif

  err = rt_sem_create(&distrib_exec, "distrib_exec", 1, S_PRIO);
  if(err != 0) {
    fprintf(stderr, "Erreur à la création du sémaphore sécurité\n");
    return EXIT_FAILURE;
  }

  // Create and launch ORDO_BUS task
  RT_TASK ordo_rt;
  task_descriptor ordo_td = {
    .task = ordo_rt,
    .task_function = ordo_task,
    .period = 125000000,
    .duration = 25000000,
    .priority = 7,
    .use_resource = 0,
  };
  create_and_start_rt_task(&ordo_td, "ORDO_BUS");

  RT_TASK distrib_rt;
  task_descriptor distrib_td = {
    .task = distrib_rt,
    .task_function = distrib_task,
    .period = 125000000,
    .duration = 25000000,
    .priority = 6,
    .use_resource = 1,
  };
  create_and_start_rt_task(&distrib_td, "DISTRIB_DONNEES");

  RT_TASK pilotage_rt;
  task_descriptor pilotage_td = {
    .task = pilotage_rt,
    .task_function = rt_task,
    .period = 250000000,
    .duration = 25000000,
    .priority = 5,
    .use_resource = 1,
  };
  create_and_start_rt_task(&pilotage_td, "PILOTAGE");

  RT_TASK radio_rt;
  task_descriptor radio_td = {
    .task = radio_rt,
    .task_function = rt_task,
    .period = 250000000,
    .duration = 25000000,
    .priority = 4,
    .use_resource = 0,
  };
  create_and_start_rt_task(&radio_td, "RADIO");

  RT_TASK camera_rt;
  task_descriptor camera_td = {
    .task = camera_rt,
    .task_function = rt_task,
    .period = 250000000,
    .duration = 25000000,
    .priority = 3,
    .use_resource = 0,
  };
  create_and_start_rt_task(&camera_td, "CAMERA");

  RT_TASK mesures_rt;
  task_descriptor mesures_td = {
    .task = mesures_rt,
    .task_function = rt_task,
    .period = 5000000000,
    .duration = 50000000,
    .priority = 2,
    .use_resource = 1,
  };
  create_and_start_rt_task(&mesures_td, "MESURES");

  RT_TASK meteo_rt;
  task_descriptor meteo_td = {
    .task = meteo_rt,
    .task_function = rt_task,
    .period = 5000000000,
    .duration = 60000000,
    .priority = 1,
    .use_resource = 1,
  };
  create_and_start_rt_task(&meteo_td, "METEO");

  while(1) continue;

  return EXIT_SUCCESS;
}
```
